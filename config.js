//Carga la configuración del sitio
const Config = {
    Constants: require('./config/Constants.js'),
    DatabaseMaster: require('./config/Database.js'),
    Email: require('./config/Email.js'),
    Notification: require('./config/Notification.js'),
    SDKs: require('./config/SDKs.js'),
    System: require('./config/System.js'),
    Storage: require('./config/Storage.js'),
    MoneyFormat : require('./config/MoneyFormat.js'),
    Lang: require('./config/Lang.js')
};
module.exports = Config;

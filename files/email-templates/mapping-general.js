//composición del template
module.exports = {
    es: {
        header: 'common/header-es.html',
        content: 'content/es',
        footer: 'common/footer-es.html'
    },
    en: {
        header: 'common/header-en.html',
        content: 'content/en',
        footer: 'common/footer-en.html'

    }

};
  module.exports = {
    1: {
      symbol: '', // este no se utilizara xq se mete directamente al template por variable
      decimal: '',
      thousand: '',
      precision: 0,
      format: '%s %v' // %s is the symbol and %v is the value
    },
    2: {
      symbol: '', // este no se utilizara xq se mete directamente al template por variable
      decimal: '',
      thousand: ',',
      precision: 0,
      format: '%s %v' // %s is the symbol and %v is the value
    },
    3: {
      symbol: '', // este no se utilizara xq se mete directamente al template por variable
      decimal: '',
      thousand: '.',
      precision: 0,
      format: '%s %v' // %s is the symbol and %v is the value
    },
    4: {
      symbol: '', // este no se utilizara xq se mete directamente al template por variable
      decimal: '.',
      thousand: '',
      precision: 2,
      format: '%s %v' // %s is the symbol and %v is the value
    },
    5: {
      symbol: '', // este no se utilizara xq se mete directamente al template por variable
      decimal: '.',
      thousand: ',',
      precision: 2,
      format: '%s %v' // %s is the symbol and %v is the value
    },
    6: {
      symbol: '', // este no se utilizara xq se mete directamente al template por variable
      decimal: '',
      thousand: ',',
      precision: 2,
      format: '%s %v' // %s is the symbol and %v is the value
    },
    7: {
      symbol: '', // este no se utilizara xq se mete directamente al template por variable
      decimal: ',',
      thousand: '.',
      precision: 2,
      format: '%s %v' // %s is the symbol and %v is the value
    }
  }

/**
 *Configuración general del sistema
 */
module.exports = {
    default: 'es',
    available: ['es', 'en'],
    langId: { es: 1, en: 2 },
    errors: {
        es: require('../api/errors/es.js'),
        en: require('../api/errors/en.js')
    },
    email: {
        es: {
            bienvenida: {
                subject: 'Bienvenido a ',
                template: 'email-tipo-1-bienvenido'
            },
            bienvenidaFacebook: {
                subject: 'Bienvenido a ',
                template: 'email-tipo-1-bienvenido-facebook'
            },
            cambiarPassword: {
                subject: 'Solicitud de Cambio de Contraseña - ',
                template: 'email-tipo-1-cambio-contraseña'
            },
            servicioAlUsuario: {
                subject: 'Servicio al cliente',
                template: 'email-tipo-1-servicio-al-cliente'
            },
            acumular_puntos: {
                subject: 'Acumulación de puntos',
                template: 'email-tipo-3-acumular'
            },            
            redimir_puntos: {
                subject: 'Canjeo exitoso',
                template: 'email-tipo-3-redimir', 
            },
            cambio_nivel: {
                subject: 'Cambio de nivel',
                template: 'email-tipo-2-aviso-cambio-nivel', 
            },
            codigoVerificacion:{
                subject: 'Código de verificación',
                template: 'email-tipo-1-codigo-verificacion'
            },
            CodigoConfirmacion:{
                subject: 'Código de confirmación',
                template: 'email-tipo-1-codigo-verificacion'
            }
        },
        en: {
            bienvenida: {
                subject: 'Welcome to ',
                template: 'email-tipo-1-bienvenido-en'
            },
            bienvenidaFacebook: {
                subject: 'Welcome to ',
                template: 'email-tipo-1-bienvenido-en-facebook'
            },
            cambiarPassword: {
                subject: 'Password Change Request - ',
                template: 'email-tipo-1-cambio-contraseña-en'
            },
            servicioAlUsuario: {
                subject: 'Customer Service',
                template: 'email-tipo-1-servicio-al-cliente-en'
            },
            acumular_puntos: {
                subject: 'Points earned',
                template: 'email-tipo-3-acumular-en'
            },            
            redimir_puntos: {
                subject: 'Successful redemption', 
                template: 'email-tipo-3-redimir-en',  
            },
            cambio_nivel: {
                subject: 'Level change',
                template: 'email-tipo-2-aviso-cambio-nivel-en', 
            },
            codigoVerificacion:{
                subject:  'Verification code',
                template: 'email-tipo-1-codigo-verificacion-en'
            },
            CodigoConfirmacion:{
                subject:  'Confirmation code',
                template: 'email-tipo-1-codigo-verificacion-en'
            }
        }
    },
    notificacion: {
        es: {
            acumular_puntos: {
                Titulo: 'Acumulación exitosa',
                Mensaje: 'Se han acumulado la siguiente cantidad:',
                MensajeNivel: 'Y su nivel a cambiado.'
            },
            redimir_puntos: {
                Titulo: 'Canejo exitoso',
                Mensaje: 'Se han canjeado exitosamente la siguiente cantidad de productos:',
            }
        },
        en: {
            acumular_puntos: {
                Titulo: 'Successful Accumulation',
                Mensaje: 'You have earned: ',
                MensajeNivel: 'You have earned a new level'
            },
            redimir_puntos: {
                Titulo: 'Successful redemption',
                Mensaje: 'You have reedemed the following amount of products:',
            }
        }


    }
};
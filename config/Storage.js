/**
 *Configuración de almacenamiento
 */
module.exports = {
	
    s3_aws_config: {
		url : 'https://s3.amazonaws.com/lealto-master/',
        version: '2006-03-01',
        bucket: 'lealto-master',
    }
};

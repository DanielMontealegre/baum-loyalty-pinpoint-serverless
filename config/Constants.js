/**
 *Configuración de las constantes del sistema
 */
//TIPOS DE TRANSACCION DE PDL => 1 - Acreditacion, 2 - Acreditacion Manual, 3 - Redencion, 4 -Vencimiento
module.exports = {

  root: __dirname.replace(/\\/g, '/').replace('config', ''),
  lamba_tmp: '/tmp/', // cambiar esto por el root __dirname.replace(/\\/g, '/').replace('config', '') para hacer pruebas locales

  useIsoWeekDay: ((process.env.useIsoWeekDay == 'true' || process.env.useIsoWeekDay == 1 || process.env.useIsoWeekDay == '1' || process.env.useIsoWeekDay == true)?  true : false),

  stores_app: {
    appstore: 'utilities/email/stores/appstore.gif',
    googleplay: 'utilities/email/stores/googleplay.gif',
    lealtoLogo: 'utilities/email/lealto/lealto-email.png',
    lealtoPagina : 'https://www.lealto.com',
    lealtoPaginaWWW : 'www.lealto.com'
  },
  estadoPreRegistro:{
    deshabilitado: 0,
    activo: 1,
    pendiente: 2,
    asociado: 3
  },
  tablaConfiguraciones: {
    id: "1"
  },
  sys_label_idioma: 'sys_label_idioma',
  //1-Sellos, 2-Cashback, 3-Premios,4-Sellos Niveles ,5-Cashback Niveles , 6-Premios Niveles
  tipoDePlan: {
    sellos: 1,
    cashback: 2,
    premios: 3,
    sellosNiveles: 4,
    cashbackNiveles: 5,
    premiosNiveles: 6
  },
  tipoRegalia:{
    bievenida: 'regalia-bienvenida',
    codigo: 'regalia-codigo',
    invitacion: 'regalia-invitacion',
    encuesta: 'regalia-encuesta'
  },
  tipoTransaccion: {
    Acreditacion: 1,
    AcreditacionManual: 2,
    Redencion: 3,
    Vencimiento: 4,
    AcreditacionBonus: 5,
    AcreditacionCupon: 6,
    Donacion: 7,
    AcreditacionEncuesta: 8,

    Acreditacion_id_label_titulo: 1,
    AcreditacionManual_id_label_titulo: 2,
    Redencion_id_label_titulo: 3,
    Vencimiento_id_label_titulo: 4,
    AcreditacionBonus_id_label_titulo: 5,
    AcreditacionCupon_id_label_titulo: 6,
    Donacion_id_label_titulo: 7,
    AcreditacionEncuesta_id_label_titulo: 8,

    Acreditacion_id_label_descripcion: 8,
    AcreditacionManual_id_label_descripcion: 9,
    Redencion_id_label_descripcion: 10,
    Vencimiento_id_label_descripcion: 11,
    AcreditacionBonus_id_label_descripcion: 12,
    AcreditacionCupon_id_label_descripcion: 13,
    Donacion_id_label_descripcion: 14,
    AcreditacionEncuesta_id_label_descripcion: 15,
  },
  tipoWebhooks:{
    usuario: 'USUARIOS_WEBHOOKS',
    acumular: 'ACUMULAR_WEBHOOKS',
    canjear:'CANJEAR_WEBHOOKS',
    cupones:'CUPONES_WEBHOOKS',
    solicitudes:'SOLICITUDES_WEBHOOKS',
  },
  tipoWebhookDescripcion:{
      usuario:{
        creado: 'user.created',
        actualizado: 'user.updated',
        sincronizado: 'user.synchronized',
        login: 'user.login',
        loginFacebook: 'user.loginFacebook',
        actulizacionNotificaciones: 'user.updateNotificaciones',
        codigoUsuarioActualizado: 'user.codeUpdated',
      },
      acumulacion:{
        regalia: 'acum.regalia',
        normal: 'acum.normal',
        especifica: 'acum.especifica',
        manual: 'acum.manual'
      }
  },
};

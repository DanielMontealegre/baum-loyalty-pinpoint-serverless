# Status funciones lambda (funcionalidad de las lambdas)
- [✓] CognitoUserPoolTrigger - postConfirmationFunc:thumbsup:
- [✓] CognitoUserPoolTrigger - preTokenGenerationFunc:thumbsup:  
- [✓] CognitoUserPoolTrigger - customMessageFunc:thumbsup:

# Status template de deployment (automatizacion de configuraciones etc) 
- [✓] CognitoUserPool :thumbsup:
- [✓] CognitoUserPoolClient :thumbsup:
- [✓] CognitoUserPoolIdentityPool :thumbsup:
- [✕] CognitoIdentityProviderFacebook (Hay que configurarlo manual en la consola de cognito o por sdk) :bangbang:
- [✓] CognitoUserPoolTrigger - postConfirmationFunc :thumbsup:
- [✕] CognitoUserPoolTrigger - preTokenGenerationFunc (Hay que configurarlo manual en el CognitoUserpool o por sdk) :bangbang:
- [✓] CognitoUserPoolTrigger - customMessageFunc :thumbsup:
- [✓] IAMRole-SDk (Para el api y admin) :thumbsup:
- [✓] IAMROLE-LambdaTriggers (role para triggers de cognito) :thumbsup:
- [✓] Pinpoint (Se utiliza una lambda para crear por sdk el app de pinpoint, si se meten las variablas como json en el SSM de AWS los canales de apns,apnssanbox y correo seran configurados tmb) :thumbsup:
>Leer esto para configurar desde consola pinpoint de apns y apnssanbox https://docs.aws.amazon.com/pinpoint/latest/userguide/channels-mobile-manage.html#channels-mobile-manage-apns

#Deployment solo funciones
### - `serverless deploy function --function myFunction `
### -  `serverless deploy function --function myFunction --update-config` (actualiza sola la configuracion)

#Deployment full
### - `git clone` 
### - `npm install`  
### - `npm install -g serverless `
### - `serverless config credentials --provider aws --key EXAMPLE --secret EXAMPLE --profile -o` // hace override del defaultprofile 
### - `node app-create-ssm.js` // Un pequeño cli para crear los SSM(Secure store parameters) necesarios para el funcionamiento de lealto plus AWS.  Se le dara la opcion de utiliza un json config, ver el [example.json](example-config.json) (todos los keys son necesarios)
### - `serverless deploy`
### - Despues del deploy hay que ir a **Cognito** y meterse en el recien creado **userPool** y configurar el **TRIGGER** de **PRE TOKEN GENERATION** (CF No soporta setear este trigger aun) 
### - Por el momento no se ha podido poner a **FACEBOOK** como un **external federated identity providers**, por lo cual hay que meterse al pool de cognito y configurarlo. Se trato de hacerlo con custom:resource y una lambda pero no resulto. 
### - Despues hay que ir a la cuenta de **AWS master de lealto** y editar la politica llamada *Lealto-API-CrossAcount-assumeRolePolicy* y agregar este Statement al array de Statements: 
>${self:service} se refiere al service en el serverles.yml 

`  {
            "Effect": "Allow",
            "Action": "sts:AssumeRole",
            "Resource": "arn:aws:iam::**id dela cuenta de este lealto plus**:role/**{Nombre con el que se creo el role en esta cuenta de lealto plus por default se llama ${self:service}-SDK-Role}**"
    }` 


#Remove stack
> Remueve el stack de cloudformation(todo lealto plus) y la aplicacion de pinpoint

- `delete pinpoint app: aws pinpoint delete-app --application-id APP_ID`
- `serverless remove`



#SSM Manual

> Los nombres tiene que tener el siguiente formato */${self:service}-${self:provider.stage}/nombre* (el service y stage estan dentro del serverless.yml). 
**Es imporante respetar este formato porque los IAM-ROles estan basados en el.**
> PATH_APNS y PATH_APNS_SANDBOX debe ser un json strigify de:`{
    "BundleId": "STRING_VALUE",
    "Certificate": "STRING_VALUE",
    "DefaultAuthenticationMethod": "STRING_VALUE'",
    "PrivateKey": "STRING_VALUE",
    "TeamId": "STRING_VALUE",
    "TokenKey": "STRING_VALUE",
    "TokenKeyId": "STRING_VALUE"
  }`, y PATH_GCM debe ser un string con el **apikey** de GCM. Ejemplo de esto en pinpointConfig.json. Hay que hacer escape de caracteres especiales como "",/, : etc. **Se recomienda hacer la configuracion de apns,apnssanbox y email desde la pagina de pinpoint**

    - aws configure (Por si no se tiene configurado el cli)
    - aws ssm put-parameter --name /LealtoPlus-test/DB_WRITE_HOST --type String --value myString --overwrite 
    - aws ssm put-parameter --name /LealtoPlus-test/DB_READ_HOST --type String --value myString --overwrite 
    - aws ssm put-parameter --name /LealtoPlus-test/DB_NAME --type String --value myString --overwrite 
    - aws ssm put-parameter --name /LealtoPlus-test/DB_USER --type String --value myString --overwrite 
    - aws ssm put-parameter --name /LealtoPlus-test/DB_PASS --type String --value myString --overwrite 
    - aws ssm put-parameter --name /LealtoPlus-test/DB_PORT --type String --value myString --overwrite 
    - aws ssm put-parameter --name /LealtoPlus-test/FACEBOOK_APP_ID --type String --value myString --overwrite 
    - aws ssm put-parameter --name /LealtoPlus-test/PATH_APNS --type String --value {\"BundleId\":\"STRING_VALUE\",\"Certificate\":\"STRING_VALUE\",\"DefaultAuthenticationMethod\":\"STRING_VALUE\",\"PrivateKey\":\"STRING_VALUE\",\"TeamId\":\"STRING_VALUE\",\"TokenKey\":\"STRING_VALUE\",\"TokenKeyId\":\"STRING_VALUE\"} --overwrite 
    - aws ssm put-parameter --name /LealtoPlus-test/PATH_APNS_SANDBOX --type String --value {\"BundleId\":\"STRING_VALUE\",\"Certificate\":\"STRING_VALUE\",\"DefaultAuthenticationMethod\":\"STRING_VALUE\",\"PrivateKey\":\"STRING_VALUE\",\"TeamId\":\"STRING_VALUE\",\"TokenKey\":\"STRING_VALUE\",\"TokenKeyId\":\"STRING_VALUE\"} --overwrite 
    - aws ssm put-parameter --name /LealtoPlus-test/PATH_GCM --type String --value myString --overwrite 

#Lambda filtrado de segmento de pinpoint
> Si al tratar de crear una campaña con filtrado por lambda da este error : Could not properly invoke Lambda function specified in hook´. Se debe correr esto por linea de comando

```
aws lambda add-permission \
 --function-name function-name \
 --statement-id sid \
 --action lambda:InvokeFunction \
 --principal pinpoint.us-east-1.amazonaws.com \
 --source-arn arn:aws:mobiletargeting:us-east-1:account-id:/apps/application-id/campaigns/campaign-id
```


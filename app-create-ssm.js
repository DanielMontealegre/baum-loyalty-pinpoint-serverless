const inquirer = require('inquirer');
const p12decoder = require('./api/helpers/p12decoder.helper.js');
const fs = require('fs-extra');
const path = require('path');
const ora = require('ora');
const spinner = ora('');
const { execSync } = require('child_process');
const yaml = require('js-yaml');
const AWS = require('aws-sdk');

const CONFIG_FILE = 'FINAL-CONFIG.json';
const OUTPUT= __dirname + `/output/`;

const PATH_APNS_CERTIFICATE = 'APNS_CERTIFICATE';
const PATH_APNS_PRIVATEKEY = 'APNS_PRIVATEKEY';
const PATH_APNS_IS_SANDBOX = 'APNS_IS_SANDBOX';
const PATH_GCM_NAME = 'GCM_KEY';
const PATH_EMAIL_NAME = 'EMAIL_CONFIG';

const KEYS_FILE = ['overwrite','DB_NAME','DB_USER','DB_PASS','DB_PORT','DB_WRITE_HOST','DB_READ_HOST','FACEBOOK_APP_ID','AWS_ACCOUNT_ID','GCM_KEY','APNS_CERTIFICATE','APNS_CERTIFICATE_PASSWORD','isAPNSSandbox','FromAddress','Identity','RoleArn','LEALTO_CLIENT_ID']

const NAMES_TO_IGNORE = ['filePath','password','isAPNSSandbox','overwrite','useFile','FromAddress','Identity','RoleArn']; // names to be ignored once we start sending the answers to SSM 



//validator
const validateFilePath = (filepath) => {
    let result = false;
    if (filepath) {
        result = fs.existsSync(filepath);
    }
    return result || 'file path must be valid';
};

const validateJSONFile = (filepath) => {
    let result = false;
    let errorMsj = 'file path must be a valid json file (.json)';
    if (filepath) {
        result = fs.existsSync(filepath);
        var ext = path.extname(filepath);
        if(result){
            if(ext !== '.json'){
                result = false;
            }else{
                let jsonFile = fs.readFileSync(filepath);
                result = validateJSON(jsonFile)
            }
        }
    }
    return result || errorMsj;
}

const validateJSON = (body) => {
  try {
    var data = JSON.parse(body);
    // if came to here, then valid
    return true;
  } catch(e) {
    // failed to parse
    return false;
  }
}

const valiateRequired = (input) => {

    return (!input || input === '')? 'Field required': true;
}


//conditions
const isNotFromFile = (answers) => {
    const { useFile } = answers;
    return !useFile;
}

const fromFile = (answers) => {
    const { useFile } = answers;
    return useFile;
}


async function run() {
    try {
        const questions = [{
                name: 'awsProfile',
                type: 'input',
                message: 'AWS shared credentials profile (defaults to "default" profile): ',
                default: 'default'
            },
            {
                name: 'awsRegion',
                type: 'input',
                message: 'AWS region (defaults to "us-east-1"): ',
                default: 'us-east-1'
            },
            {
                name: 'useFile',
                type: 'confirm',
                message: 'Use config file (take example-config.json as example): ',
                validate: valiateRequired
            },
            {
                name: 'fileLocation',
                type: 'input',
                message: 'Config file location: ',
                validate: validateJSONFile,
                when: fromFile
            },
            {
                name: 'overwrite',
                type: 'confirm',
                message: 'Overwrite SSM parameters: ',
                validate: valiateRequired,
                when:  isNotFromFile
            },
            {
                name: 'LEALTO_CLIENT_ID',
                type: 'input',
                message: 'Lealto client id: ',
                validate: valiateRequired,
                when:  isNotFromFile            
            },
            {
                name: 'DB_NAME',
                type: 'input',
                message: 'Lealto master database name: ',
                validate: valiateRequired,
                when:  isNotFromFile            
            },
            {
                name: 'DB_USER',
                type: 'input',
                message: 'Lealto master database username: ',
                validate: valiateRequired,
                when:  isNotFromFile

            },
                        {
                name: 'DB_PASS',
                type: 'input',
                message: 'Lealto master username password: ',
                validate: valiateRequired,
                when:  isNotFromFile
            },
            {
                name: 'DB_PORT',
                type: 'input',
                message: 'Lealto master database port: ',
                validate: valiateRequired,
                when:  isNotFromFile
            },
            {
                name: 'DB_WRITE_HOST',
                type: 'input',
                message: 'Lealto master database write hostname: ',
                validate: valiateRequired,
                when:  isNotFromFile
            },
            {
                name: 'DB_READ_HOST',
                type: 'input',
                message: 'Lealto master database read hostname: ',
                validate: valiateRequired,
                when:  isNotFromFile
            },
           {
                name: 'FACEBOOK_APP_ID',
                type: 'input',
                message: 'Facebook app id: ',
                validate: valiateRequired,
                when:  isNotFromFile
            },
            {
                name: 'AWS_ACCOUNT_ID',
                type: 'input',
                message: 'Lealto main AWS account id: ',
                validate: valiateRequired,
                when:  isNotFromFile
            },
            {
                name: 'GCM_KEY',
                type: 'input',
                message: 'FCM Api key (GCM): ',
                validate: valiateRequired,
                when:  isNotFromFile
            },
            {
                name: 'filePath',
                type: 'input',
                message: 'APNS certificate file path (.p12): ',
                validate: validateFilePath,
                when:  isNotFromFile
            },
            {
                name: 'password',
                type: 'input',
                message: 'The certificate password (if any): ',
                when:  isNotFromFile
            },
            {
                name: 'isAPNSSandbox',
                type: 'confirm',
                message: 'Is APNSSandbox : ',
                when:  isNotFromFile
            },
            {
              name: 'FromAddress',
              type: 'input',
              message: "The 'From' Email address used to send emails by pinpoint: ",
              when:  isNotFromFile
            },
            {
              name: 'Identity',
              type: 'input',
              message: 'The ARN of an identity verified with SES: ',
              when:  isNotFromFile
            },
            {
              name: 'RoleArn',
              type: 'input',
              message: "The ARN of an IAM Role used to submit events to Mobile notifications' event ingestion service: ",
              when:  isNotFromFile
            },
        ];
        const answersPrompt = await inquirer.prompt(questions);

        const { useFile, fileLocation, awsProfile, awsRegion } = answersPrompt;

        const credentials = new AWS.SharedIniFileCredentials({ profile: awsProfile });

        const SSM = new AWS.SSM({ apiVersion: '2014-11-06', credentials, region: awsRegion  });

        answers = (useFile)? readFromFile(fileLocation) : answersPrompt;

        spinner.start('Creating CONFIG.json and Updating SSM.');
        const { service, stage } = getServiceConfig();
        const { FromAddress, Identity, RoleArn, isAPNSSandbox, overwrite , filePath , password} = answers;

        const { Certificate, PrivateKey } = await p12decoder.run({ filePath, password });

        const emailRequest = { FromAddress,Identity,RoleArn };


        answers = addObjetToAnswers(answers,[
            { name: PATH_EMAIL_NAME, value: JSON.stringify(emailRequest)  },
            { name: PATH_APNS_CERTIFICATE, value: Certificate  },
            { name: PATH_APNS_PRIVATEKEY, value: PrivateKey  },
            { name: PATH_APNS_IS_SANDBOX, value:  isAPNSSandbox }
        ]);
        const finalResponse = { ...processAnswersToSSM(answers, service, stage ,overwrite, SSM) };

        writeResultToFile(finalResponse);
        spinner.succeed(`The ${CONFIG_FILE} has been successfully created. Location: ${path.resolve(path.resolve(OUTPUT, CONFIG_FILE))}. \n SSM successfully updated.`);

        return;
        }catch (error) {
            spinner.fail('Critical error');
            console.log(error);
        }
};

//OPERATIONS OVER ANSWERS
const processAnswersToSSM = (answers, service, stage, overwrite , SSM) => {
    return Object.keys(answers).reduce((r,name,i) => {
        if(!NAMES_TO_IGNORE.some((e,i) => (e == name)) )  {
            const SSM_PATH = `/${service}-${stage}/${name}`;
            const value = (typeof answers[name] == 'string' && name != PATH_APNS_CERTIFICATE && name != PATH_APNS_PRIVATEKEY)? answers[name].replace(/\s/g, "") : answers[name];  // replace whitespaces
            sendSSMPutParameter(SSM_PATH,value,overwrite, SSM);
            const auxObj = { [name]: { value, ssm_path: SSM_PATH } }
            return Object.assign({}, r, auxObj);
        }
        return r;   
    },{ })
};

const addObjetToAnswers = (answers,objects) => {
    const allObjets = objects.reduce((r,e) =>  ({...r, [e.name]: e.value}),{});
    return { ...answers, ...allObjets };
};



//SSM OPERATIONS
const sendSSMPutParameter = async (ssm_path, value, overwrite = false , SSM) => {
    try{
        const isObj = (ssm_path.includes(PATH_EMAIL_NAME))? true :false;
        const isAPMS = (ssm_path.includes(PATH_APNS_CERTIFICATE) || ssm_path.includes(PATH_APNS_PRIVATEKEY))? true : false

        var params = {
          Name: ssm_path, /* required */
          Type: 'String', /* required */
          Value: `${((isObj)? JSON.stringify(value) : `${((isAPMS)? `${(value)}` : value)}`)}`, /* required */
          //AllowedPattern: 'STRING_VALUE',
          Description: 'Lealto SSM parameter',
          //KeyId: 'STRING_VALUE',
          Overwrite: overwrite 
        };
        const result = await SSM.putParameter(params).promise();
        console.log(`${ssm_path} => ${JSON.stringify(result)}`);
        return result;
    }catch (error) {
            console.log(`Error ${ssm_path} => ${error.stderr} ${error}`);
            return;
    }
};

// files operations
const readFromFile = (file) => {
    if (!fs.existsSync(file)) {
        throw new Error(`File doesnt exist ${file}`);
    } else {
        let jsonFile = fs.readFileSync(file);
        const json = JSON.parse(jsonFile);
        const validKeys = checkKeys(json, KEYS_FILE);
        if (!validKeys) {
            throw new Error(`Invalid keys. Valid keys: ${KEYS_FILE}`);
            return;
        }
        const validCertificateFile = validateFilePath(json['APNS_CERTIFICATE']);
        if (!validCertificateFile) {
            throw new Error(`Invalid APNS_CERTIFICATE : ${json['APNS_CERTIFICATE']}`);
            return;
        }

        return transformObj(json);
    }
};

const writeResultToFile = (json) => {
    if (fs.existsSync(path.resolve(OUTPUT, CONFIG_FILE)) ) fs.unlinkSync(path.resolve(OUTPUT, CONFIG_FILE));
    let jsonFile = JSON.stringify(json, null, '\t');
    fs.writeFileSync(path.resolve(path.resolve(OUTPUT, CONFIG_FILE)), jsonFile, 'utf8');
    return true;
};

const getServiceConfig = () => {
        const doc = yaml.safeLoad(fs.readFileSync(__dirname + '/serverless.yml', 'utf8'));
        const { service, provider } = doc;
        const { stage } = provider;
        return { service, stage };
};

//obj validators
const checkKeys = (obj, keys) => {
    const objKeys = Object.keys(obj);
    return objKeys.some((e, i) => keys.some((e2, i) => e2 !== e) || (typeof obj[e] !== 'string'));
};

const transformObj = (obj) => {
    const replacements = {
        APNS_CERTIFICATE: 'filePath',
        APNS_CERTIFICATE_PASSWORD: 'password'
    };
    const objKeys = Object.keys(obj);
    return objKeys.reduce((r, key, i) => {
        if (replacements.hasOwnProperty(key)) {
            const aux = {
                [replacements[key]]: obj[key] }
            return Object.assign({}, r, aux);
        }
        return Object.assign({}, r, {
            [key]: obj[key] });
    }, {})
};


run();


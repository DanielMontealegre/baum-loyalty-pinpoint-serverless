//para las varialbes locales en local no en lambda, testing

/*
const path = require('path');
const env_path = path.resolve(__dirname, '.env');
require('dotenv').config({ path: env_path });
*/

const mkdirp = require('mkdirp');
const moment = require('moment-timezone');

mkdirp.sync('/tmp/files/email-templates/final/es','0777'); // inicializamos las carpteas para los emails
mkdirp.sync('/tmp/files/email-templates/final/en','0777'); // inicializamos las carpteas para los emails

//default timezone costa-rica en vez del timezone de la lambda
moment.tz.setDefault("America/Costa_Rica");


/**
 * Initialize configurations from AWS SSm or .env . this has to be before anything that uses configurations file. ENVIRONMENT VARIABLES
 *
 *
 *
 */
const SSMHelper = require('./api/helpers/Enviroment.helper.js');

const PostConfirmation = require('./api/handler/postConfirmation.handler.js');
const PreTokenGenerationHandler = require('./api/handler/preTokenGeneration.handler.js');
const CustomMessageHandler = require('./api/handler/customMessage.handler.js');
const PreSignUpHandler = require('./api/handler/preSignUp.handler.js');
const UserMigrationHandler = require('./api/handler/userMigration.handler.js');
const PinpointCreatorHandler = require('./api/handler/pinpointCreator.handler.js');
const UpdateAWSConfigLealtoHandler = require('./api/handler/updateAWSConfigLealto.handler.js');
const FilterSegmentHandler = require('./api/handler/filterSegment.handler.js');

const errorCodeSSM = 'ErrorSSM-0000'; //Couldnt load enviroment variables! Need valid .env for SSM or local

module.exports.postConfirmation = async (event) => {
	const resultConfig = await SSMHelper.getDBConfig();
	if(!resultConfig.valid)  throw new Error(errorCodeSSM);
	return PostConfirmation.handler(event,resultConfig.databaseConfig);
};

module.exports.preTokenGeneration = async (event) => {
	const resultConfig = await SSMHelper.getDBConfig();
	if(!resultConfig.valid)  throw new Error(errorCodeSSM);
	return PreTokenGenerationHandler.handler(event,resultConfig.databaseConfig);
};

module.exports.customMessage = async (event) => {
	const resultConfig = await SSMHelper.getDBConfig();
	if(!resultConfig.valid)  throw new Error(errorCodeSSM);
	return CustomMessageHandler.handler(event,resultConfig.databaseConfig);
};

module.exports.preSignUp = async (event) => {
	const resultConfig = await SSMHelper.getDBConfig();
	if(!resultConfig.valid)  throw new Error(errorCodeSSM);
	return PreSignUpHandler.handler(event,resultConfig.databaseConfig);
};

module.exports.userMigration = async (event) => {
	const resultConfig = await SSMHelper.getDBConfig();
	if(!resultConfig.valid)  throw new Error(errorCodeSSM);
	return UserMigrationHandler.handler(event,resultConfig.databaseConfig);
};

module.exports.filterSegmentHandler = async (event) => {
	const resultConfig = await SSMHelper.getDBConfig();
	if(!resultConfig.valid)  throw new Error(errorCodeSSM);
	return FilterSegmentHandler.handler(event,resultConfig.databaseConfig);
};

module.exports.updateAWSConfigLealto = (event,context) => {
	UpdateAWSConfigLealtoHandler.handler(event,context); // esto se hace asi porque es para CF y se usa el context para devolver la jugada
};

module.exports.pinpointCreator = (event,context) => {
	console.log(`====event===> ${JSON.stringify(event)} =====context====> ${JSON.stringify(context)} ========>`);
	PinpointCreatorHandler.handler(event,context); // esto se hace asi porque es para CF y se usa el context para devolver la jugada
};


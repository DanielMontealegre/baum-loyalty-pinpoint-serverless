//Packages generales
const Sequelize = require("sequelize");
const Op = Sequelize.Op;
const Promise = require("bluebird");
const R = require("ramda");
const _ = require("lodash");
//sequelize instance master y init de cliente
const sequelizeInit = require("../core/Sequelize.js");

//helpers
const GlobalHelper = require("../helpers/Global.helper.js");
const EmailHelper = require("../helpers/Email.helper.js");
const SecurityHelper = require("../helpers/Security.helper.js");
const phoneUtil = require("google-libphonenumber").PhoneNumberUtil.getInstance();
const PNF = require("google-libphonenumber").PhoneNumberFormat;

//config
const Config = require("../../config.js");
const Constants = Config.Constants;
const LangConfig = Config.Lang;
const url_change_pass = Constants.url_change_pass;
const ESTADO_PRE_REGISTRO = Constants.estadoPreRegistro;
const TIPO_WEBHOOKS = Constants.tipoWebhooks;
const DESC_TIPO_WEBHOOK = Constants.tipoWebhookDescripcion;
const TIPO_REGALIA = Constants.tipoRegalia;
//Modelos cliente
const Usuario = require("../models/Usuario.model.js");
const Genero = require("../models/Genero.model.js");
const Nivel = require("../models/Nivel.model.js");
const Configuracion = require("../models/Configuracion.model.js");
const Saldo = require("../models/Saldo.model.js");
const invitacionRegalia = require("../models/Invitacion.model.js");
const TransaccionPremioModel = require("../models/TransaccionPremio.model.js");
const TransaccionSaldoModel = require("../models/TransaccionSaldo.model.js");
const UsuariosInfoExtraModel = require("../models/UsuarioInfoExtra.model.js");
const PreRegistroModel = require("../models/PreRegistro.model.js");
const SysColaAccionesModel = require("../models/SysColaAcciones.model.js");

const Emails = require("../models/Emails.model.js");
//Packages
const moment = require("moment-timezone");

//services
const AWS = require("aws-sdk");
const pinpoint = new AWS.Pinpoint({});
const cognito = new AWS.CognitoIdentityServiceProvider({});

// TIMES atributes
const EARLY_MORNING = "EarlyMorning";
const BREAKFAST_TIME = "BreakfastTime";
const MORNING = "Morning";
const LAUNCH_TIME = "LaunchTime";
const AFTERNOON = "Afternoon";
const DINNER_TIME = "DinnerTime";
const NIGTH = "Night";

//AGE attributes
const KID = "Kid";
const YOUNG = "Young";
const ADULT = "Adult";
const ELDERLY = "Elderly";

///RAMDA FUNCTIONS

const isValidValue = R.complement(R.either(R.isNil, R.isEmpty));
const isNumber = value => {
  return _.isNumber(value);
};
const roundToTwo = num => {
  return Math.round((num + 0.00001) * 100) / 100;
};

class UsuarioController {
  constructor(
    sequelizeIntance,
    sequelizeMasterCon,
    config_cliente,
    lenguaje_actual = "es"
  ) {
    //lenguaje actual
    this.sequelizeIntance = sequelizeIntance;
    this.sequelizeMasterCon = sequelizeMasterCon;
    this.lenguaje_actual = lenguaje_actual;
    this.config_cliente = config_cliente;
    this.idCliente = config_cliente.idCliente;
    this.cognito_userpool_id = config_cliente
      ? config_cliente.AWSConfig.user_pool_id
      : "";
    this.pl_tipo = config_cliente.PlanConfig.pl_tipo;
    //modelos
    this.UsuarioModeloInstancia = Usuario.init(sequelizeIntance);
    this.GeneroModeloInstancia = Genero.init(sequelizeIntance);
    this.EmailsModeloInstancia = Emails.init(sequelizeIntance);
    this.NivelInstancia = Nivel.init(sequelizeIntance);
    this.ConfiguracionInstancia = Configuracion.init(sequelizeIntance);
    this.SaldoInstancia = Saldo.init(sequelizeIntance);
    this.TransaccionPremio = TransaccionPremioModel.init(sequelizeIntance);
    this.TransaccionSaldo = TransaccionSaldoModel.init(sequelizeIntance);
    this.invitacionRegaliaInstancia = invitacionRegalia.init(sequelizeIntance);
    this.UsuariosInfoExtra = UsuariosInfoExtraModel.init(sequelizeIntance);
    this.PreRegistroInstancia = PreRegistroModel.init(sequelizeIntance);
    this.SysColaAcciones = SysColaAccionesModel.init(sequelizeIntance);
    this.UsuarioModeloInstancia.associate(this.GeneroModeloInstancia);
    //errores
    this.Errors = Config.Lang.errors[this.lenguaje_actual];
    this.ErrorSystem = this.Errors.system;
    this.ErrorStrings = this.Errors.strings;
    this.ErrorMessages = this.Errors.codes.Usuario;
    this.ErrorMessagesConfig = this.Errors.codes.Configuracion;
    //funciones
    this.registrarTransaccionPuntosIniciales = this.registrarTransaccionPuntosIniciales.bind(
      this
    );
    this.getRegaliasConfig = this.getRegaliasConfig.bind(this);
    this.AppRegistrarUsuario = this.AppRegistrarUsuario.bind(this);
    this.AppDarRegalia = this.AppDarRegalia.bind(this);
    this.AppAsignarNivelInicial = this.AppAsignarNivelInicial.bind(this);
    this.AppDarPuntosIniciales = this.AppDarPuntosIniciales.bind(this);
    this.AppUserMigration = this.AppUserMigration.bind(this);
    this.envioBienvenida = this.envioBienvenida.bind(this);
    this.setSaldoCashbackPremios = this.setSaldoCashbackPremios.bind(this);
    this.AppGetUsuarioBySub = this.AppGetUsuarioBySub.bind(this);
    this.AppAsignarSubAUsuario = this.AppAsignarSubAUsuario.bind(this);
    this.registrarEndpointEmail = this.registrarEndpointEmail.bind(this);
    this.sincronizarUsuario = this.sincronizarUsuario.bind(this);
    this.validarCodigoPreRegistro = this.validarCodigoPreRegistro.bind(this);
    this.guardarAccionPendiente = this.guardarAccionPendiente.bind(this);
    this.userAttributesForEndpoint = this.userAttributesForEndpoint.bind(this);
    this.toRound = this.toRound.bind(this);
  }
  AppGetUsuarioBySub(sub) {
    const { UsuarioModeloInstancia } = this;
    const where = { sub_cognito: sub, estado: 1 };
    return UsuarioModeloInstancia.findOne({ where }).catch(e => {
      console.log(e);
      return null;
    });
  }
  AppRegistrarUsuario(data, id_cliente) {
    try {
      const { sequelizeIntance } = this;
      const {
        Errors,
        ErrorSystem,
        ErrorStrings,
        ErrorMessages,
        ErrorMessagesConfig
      } = this;
      const { lenguaje_actual, config_cliente, sequelizeMasterCon } = this;
      const { UsuarioModeloInstancia } = this;
      const {
        AppDarRegalia,
        AppAsignarNivelInicial,
        AppDarPuntosIniciales,
        envioBienvenida,
        registrarEndpointEmail,
        sincronizarUsuario
      } = this;

      return sequelizeIntance
        .transaction(async transaction => {
          let codigo_invitacion = data.codigo_invitacion;
          let req_identificacion = data.identificacion
            ? data.identificacion.trim()
            : "";
          let req_email = data.email.trim().toLowerCase();

          let where = { $or: [{ email: req_email }] };

          //si viene la identificacion
          if (req_identificacion !== "") {
            where.$or.push({ identificacion: req_identificacion });
          }

          const usuario = await UsuarioModeloInstancia.findOne({
            where: { email: req_email }
          });
          if (usuario) {
            const token = await SecurityHelper.createUsuarioToken(
              sequelizeMasterCon,
              usuario.id,
              config_cliente.id_cliente,
              usuario.email,
              usuario.nombre + " " + usuario.apellidos
            );
            if (data.sub && !usuario.sub_cognito) {
              await usuario.update({ sub_cognito: data.sub });
            }
            return { valido: true, token, usuario };
          }

          const codigo_preregistro = data.codigo_preregistro;
          //si no existe, crea modelo y lo guarda
          let params = {
            sub_cognito: data.sub ? data.sub : null,
            identificacion: req_identificacion,
            id_genero: data.id_genero ? data.id_genero : null,
            tipo_identificacion: data.tipo_identificacion,
            nombre: capitalizeFirstLetter(data.nombre),
            apellidos: capitalizeFirstLetter(data.apellidos),
            email: req_email,
            telefono: data.telefono ? data.telefono : "",
            puntos_historicos: 0,
            puntos: 0,
            puntos_totaltes: 0,
            saldo_premios: 0,
            imagen_perfil: data.imagen_perfil,
            estado: 1,
            id_idioma: lenguaje_actual,
            acepta_notificaciones: true,
            fecha_nacimiento: data.fecha_nacimiento
          };

          //se guarda el usuario
          const usuarioSave = await UsuarioModeloInstancia.build(params).save();
          let esPreregistro = false;
          if (codigo_preregistro) {
            const { valido } = await sincronizarUsuario(
              usuarioSave.id,
              codigo_preregistro
            );
            esPreregistro = valido;
          }

          const token = await SecurityHelper.createUsuarioToken(
            sequelizeMasterCon,
            usuarioSave.id,
            config_cliente.id_cliente,
            usuarioSave.email,
            usuarioSave.nombre + " " + usuarioSave.apellidos
          );

          await registrarEndpointEmail(usuarioSave, data.sub);

          if (!esPreregistro) {
            await AppDarPuntosIniciales(usuarioSave).catch(err => {
              console.log(`Error AppDarPuntosIniciales => ${err}`);
            });
            await AppDarRegalia(usuarioSave, codigo_invitacion).catch(err => {
              console.log(`Error AppDarRegalia => ${err}`);
            });
            await usuarioSave.reload();
          }
          await AppAsignarNivelInicial(
            config_cliente,
            usuarioSave,
            usuarioSave.puntos
          ).catch(err => {
            console.log(`Error AppAsignarNivelInicial => ${err}`);
          });
          await usuarioSave.reload();

          const to = [{ email: usuarioSave.email, name: usuarioSave.nombre }];
          await envioBienvenida(
            config_cliente,
            to,
            usuarioSave,
            lenguaje_actual
          ); // envio de correo

          return { valido: true, token, usuario: usuarioSave };
        })
        .catch(error => {
          console.log(error);
          return {
            valido: false,
            status: 500,
            error: { mensaje: "Error inesperado - 0003 " }
          };
        });
    } catch (error) {
      console.log(error);
      return {
        valido: false,
        status: 500,
        error: { mensaje: "Error inesperado - 0003 " }
      };
    }
  }
  async sincronizarUsuario(id_usuario, codigo) {
    const {
      UsuarioModeloInstancia,
      UsuariosInfoExtra,
      PreRegistro,
      ErrorMessages
    } = this;
    const { validarCodigoPreRegistro } = this;

    let usuario = await UsuarioModeloInstancia.findOne({
      where: { id: id_usuario },
      attributes: { exclude: ["qr_code", "password", "codigo"] }
    });
    if (!usuario) {
      const response = {
        valido: false,
        error: {
          mensaje: ErrorMessages["0010"]
        }
      };
      return;
    }
    const { valido, error, data } = await validarCodigoPreRegistro(
      id_usuario,
      codigo
    );

    if (!valido) {
      return { valido, error };
    }

    await data.update({ estado: ESTADO_PRE_REGISTRO.pendiente, id_usuario });
    await usuario.update({ codigo });
    usuario = usuario.toJSON();
    usuario.codigo = codigo;
    return { valido: true };
  }
  async AppDarRegalia(usuario_invitado, codigo_invitacion) {
    const { getRegaliasConfig } = this;
    const { invitacionRegaliaInstancia } = this;

    return getRegaliasConfig(codigo_invitacion, false).then(
      configuracionYusuario => {
        if (!configuracionYusuario.valido) {
          return configuracionYusuario;
        }
        let usuario_invita = configuracionYusuario.usuario,
          configuracion = configuracionYusuario.configuracion;
        let puntos_x_aceptar_invitacion =
            configuracion.invi_puntos_x_aceptar_invitacion,
          puntos_x_invitar = configuracion.invi_puntos_x_invitar,
          fechaAux = new Date(),
          fechaVencimiento = configuracion.invi_vencimiento
            ? new Date(
                fechaAux.setMonth(
                  fechaAux.getMonth() + configuracion.invi_tiempo_vencimiento
                )
              )
            : null;

        let params = {
          id_usuario_invita: usuario_invita.id,
          id_usuario_invitado: usuario_invitado.id,
          cant_regalias_invita: configuracion.invi_cant_regalias,
          cant_regalias_invitado: configuracion.invi_cant_regalias,
          fecha_vencimiento_invita: fechaVencimiento,
          fecha_vencimiento_invitado: fechaVencimiento,
          fecha_registro: new Date()
        };
        return invitacionRegaliaInstancia.build(params).save();
      }
    );
  }
  async AppAsignarNivelInicial(config_cliente, usuario, inicio = 0) {
    const {
      NivelInstancia,
      sequelizeIntance,
      UsuarioModeloInstancia,
      ErrorMessagesConfig
    } = this;
    if (config_cliente.Plan_Cliente.niveles) {
      return NivelInstancia.findAll({
        attributes: ["id"],
        where: {
          estado: 1,
          $or: [
            {
              $and: [
                {
                  min_puntos: { $lte: inicio },
                  max_puntos: { $gte: inicio }
                }
              ]
            },
            {
              max_puntos: { $lte: inicio }
            }
          ]
        },
        order: Sequelize.literal("max_puntos ASC")
      }).then(async Niveles => {
        if (!Niveles[0]) {
          // sacamos el nivel mas bajo que haya
          const nivelMin = await sequelizeIntance.query(
            `SELECT id, MIN(min_puntos) as min_puntos
            FROM ${NivelInstancia.tableName}
            WHERE estado = 1;`,
            {
              model: NivelInstancia,
              mapToModel: true, // pass true here if you have any mapped fields
              type: Sequelize.QueryTypes.SELECT
            }
          );
          if (!nivelMin[0]) {
            return Promise.resolve(
              MODELOS_INSTANCES.ErrorMessagesConfig["0001"]
            );
          }
          Niveles[0] = nivelMin[0];
        }
        let nivel = Niveles[0];
        return UsuarioModeloInstancia.update(
          { id_nivel: nivel.id },
          { where: { id: usuario.id } }
        );
      });
    }
    return Promise.resolve(0);
  }
  async AppDarPuntosIniciales(usuario) {
    // instancia de sequelize
    const {
      registrarTransaccionPuntosIniciales,
      ConfiguracionInstancia,
      setSaldoCashbackPremios,
      guardarAccionPendiente
    } = this;
    const { idCliente, pl_tipo } = this;
    return ConfiguracionInstancia.findOne({
      attributes: [
        "id",
        "acum_vencimiento",
        "acum_puntos_iniciales",
        "acum_tiempo_vencimiento",
        "acum_nombre_puntos",
        "acum_simbolo_puntos"
      ],
      where: { id: Constants.tablaConfiguraciones.id }
    }).then(configuracion => {
      if (configuracion) {
        const tipo_plan = pl_tipo;
        const puntos_inicio = this.toRound(configuracion.acum_puntos_iniciales);
        delete usuario["password"];
        delete usuario["saldo_premios"];
        delete usuario["puntos_historicos"];
        delete usuario["puntos_totales"];
        delete usuario["puntos"];

        const funcAccion = transaccion => {
          const payload = {
            usuario,
            regalia: {
              puntos: puntos_inicio,
              numero_transaccion: transaccion.id,
              tipo: TIPO_REGALIA.bievenida
            },
            configuracion
          };
          const data = {
            id_cliente_lealto: idCliente,
            type: DESC_TIPO_WEBHOOK.acumulacion.regalia,
            payload
          };
          return guardarAccionPendiente(TIPO_WEBHOOKS.acumular, {
            data,
            host: "",
            type: "send-first-time",
            webhook: DESC_TIPO_WEBHOOK.acumulacion.regalia,
            retries: 0
          });
        };

        if (typeof puntos_inicio == "undefined" || puntos_inicio <= 0) {
          return Promise.resolve(0);
        }
        if (tipo_plan == Constants.tipoDePlan.sellos) {
          return usuario
            .increment(["puntos", "puntos_historicos", "puntos_totales"], {
              by: puntos_inicio
            })
            .then(() => {
              return registrarTransaccionPuntosIniciales(
                usuario,
                puntos_inicio,
                true
              );
            })
            .then(transaccion => funcAccion(transaccion));
          //.then(() => { return NotificacionHelper.notificarAcumulacion(configuracion)(usuario, { puntos: puntos_inicio }, configuracion, true, "registro") }); // hay que cambiarlo, lo mejor seria ponerlo dentro de MODELOS_INTANCIAS MODELOS_INSTANCES['NotificacionHelper']
        }
        if (tipo_plan == Constants.tipoDePlan.cashback) {
          return setSaldoCashbackPremios(usuario, configuracion)
            .then(puntos_inicio => {
              return registrarTransaccionPuntosIniciales(
                usuario,
                puntos_inicio,
                false
              );
            })
            .then(transaccion => funcAccion(transaccion));
          //.then(() => { return NotificacionHelper.notificarAcumulacion(configuracion)(usuario, { puntos: puntos_inicio }, configuracion, true, "registro"); }); // hay que cambiarlo, lo mejor seria ponerlo dentro de MODELOS_INTANCIAS MODELOS_INSTANCES['NotificacionHelper']
        }
        if (tipo_plan == Constants.tipoDePlan.premios) {
          return setSaldoCashbackPremios(usuario, configuracion)
            .then(puntos_inicio => {
              return registrarTransaccionPuntosIniciales(
                usuario,
                puntos_inicio,
                true
              );
            })
            .then(transaccion => funcAccion(transaccion));
          //.then(() => { return NotificacionHelper.notificarAcumulacion(configuracion)(usuario, { puntos: puntos_inicio }, configuracion, true, "registro"); }); // hay que cambiarlo, lo mejor seria ponerlo dentro de MODELOS_INTANCIAS MODELOS_INSTANCES['NotificacionHelper']
        }
      }
      return Promise.resolve(0);
    });
  }
  async AppUserMigration(
    email = "",
    password_request = "",
    isForgotPassword = false
  ) {
    const {
      UsuarioModeloInstancia,
      cognito_userpool_id,
      ErrorMessages,
      ErrorSystem
    } = this;
    try {
      const defaultMsj = "Invalid user for migration";
      const user = await UsuarioModeloInstancia.findOne({ where: { email } });
      if (!user) {
        return {
          valid: false,
          error: { mensaje: ErrorMessages["0004"] },
          status: 400,
          code: "U0004"
        };
      }
      if (
        !SecurityHelper.compareHash(password_request, user.password) &&
        !isForgotPassword
      ) {
        return {
          valid: false,
          error: { mensaje: ErrorMessages["0005"] },
          status: 400,
          code: "U0004"
        };
      }
      const funcArrayObjs = (acum, e) =>
        R.merge(acum, { [R.prop("Name", e)]: R.prop("Value", e) });
      const userAttributes = await mapUserAttributes(
        cognito_userpool_id,
        user.toJSON(),
        true
      );
      const attributes = R.reduce(funcArrayObjs, {}, userAttributes);
      attributes.username = email;
      console.log({ attributes });

      return {
        valid: true,
        error: null,
        user: user.toJSON(),
        responseCognito: {
          userAttributes: attributes,
          finalUserStatus: "CONFIRMED",
          messageAction: "SUPPRESS"
          //"desiredDeliveryMediums": [ "string", ... ],
          //"forceAliasCreation": boolean
        }
      };
    } catch (error) {
      console.log(error);
      return {
        valid: false,
        error: { mensaje: ErrorSystem["500"] },
        status: 500,
        code: "S500"
      };
    }
  }
  //Aun usuario ya creado en la base de datos le asigna el sub de cognito. solo lo hace por email y registra/update el endpoint de correo
  async AppAsignarSubAUsuario(sub = "", email = "") {
    const { UsuarioModeloInstancia, registrarEndpointEmail } = this;
    try {
      if (!isValidValue(sub) || !isValidValue(email)) {
        return false;
      }
      const user = await UsuarioModeloInstancia.findOne({ where: { email } });
      if (!user) {
        return false;
      }
      await user.update({ sub_cognito: sub });
      await registrarEndpointEmail(user, sub);
      return true;
    } catch (error) {
      console.log(error);
      return false;
    }
  }
  //utlidad
  async registrarEndpointEmail(usuario, sub) {
    const { config_cliente } = this;
    const { AWSConfig } = config_cliente;
    const { user_pool_id, pinpoint_application_id: ApplicationId } = AWSConfig;

    // return hour time
    function getTime() {
      const d = new Date();
      const time = d.getHours();
      switch (true) {
        case time > 0 && time < 5:
          return EARLY_MORNING;
        case time > 4 && time < 9:
          return BREAKFAST_TIME;
        case time > 8 && time < 11:
          return MORNING;
        case time > 10 && time < 14:
          return LAUNCH_TIME;
        case time > 13 && time < 18:
          return AFTERNOON;
        case time > 17 && time < 21:
          return DINNER_TIME;
        case time > 20 && time < 0:
          return NIGTH;

        default:
          return "NONE";
      }
    }

    // return age
    function getAge(birthday) {
      const age = moment().diff(birthday, "years");
      switch (true) {
        case age > 0 && age < 12:
          return KID;
        case age > 11 && age < 21:
          return YOUNG;
        case age > 20 && age < 60:
          return ADULT;
        case age > 59:
          return ELDERLY;

        default:
          return "NONE";
      }
    }
    const { email, fecha_nacimiento, tipo_identificacion } = usuario;
    const time = getTime();
    const age = getAge(fecha_nacimiento);
    const foreign = tipo_identificacion === "2" ? "yes" : "no";

    const paramsApp = {
      ApplicationId /* required */,
      EndpointId: email /* required */,
      EndpointRequest: {
        /* required */
        Address: email,
        ChannelType: "EMAIL",
        Demographic: {
          Timezone: "America/Costa_Rica"
        },
        EffectiveDate: moment().toISOString(),
        EndpointStatus: "ACTIVE",
        Location: {
          //City: 'STRING_VALUE',
          Country: "CR" //   ISO 3166-2:CR
          //Latitude: 0.0,
          //Longitude: 0.0,
          //PostalCode: 'STRING_VALUE',
          //Region: 'STRING_VALUE'
        },
        OptOut: "NONE",
        User: {
          UserId: sub,
          UserAttributes: await this.userAttributesForEndpoint(sub)
        }
      }
    };
    const params = { ...paramsApp, ApplicationId };
    return pinpoint
      .updateEndpoint(params)
      .promise()
      .then(r => {
        if (r) {
          r.valido = true;
          r.endpointId = email;
          return r;
        } else {
          return { valido: false, error: r };
        }
      });
    //.catch(err => {console.log(`Error in registrarEndpointEmail ${err}`); return { valido: false, error:err }});
  }
  registrarTransaccionPuntosIniciales(usuario, puntos_inicio, isPremios) {
    const { TransaccionSaldo, TransaccionPremio } = this;
    const params_transaccion = !isPremios
      ? {
          num_factura: null,
          id_usuario: usuario.id,
          monto: this.toRound(puntos_inicio),
          saldo_disponible: this.toRound(puntos_inicio),
          tipo: Constants.tipoTransaccion.AcreditacionBonus,
          id_label_titulo:
            Constants.tipoTransaccion.AcreditacionBonus_id_label_titulo,
          id_label_descripcion:
            Constants.tipoTransaccion.AcreditacionBonus_id_label_descripcion,
          id_sucursal: null,
          created_at: new Date() //cambiar
        }
      : {
          num_factura: null,
          id_usuario: usuario.id,
          id_label_nombre_premio: null,
          id_label_descripcion_premio: null,
          puntos: this.toRound(puntos_inicio),
          puntos_disponibles: this.toRound(puntos_inicio),
          tipo: Constants.tipoTransaccion.AcreditacionBonus,
          id_label_titulo:
            Constants.tipoTransaccion.AcreditacionBonus_id_label_titulo,
          id_label_descripcion:
            Constants.tipoTransaccion.AcreditacionBonus_id_label_descripcion,
          id_sucursal: null,
          created_at: new Date()
        };
    return !isPremios
      ? TransaccionSaldo.build(params_transaccion).save()
      : TransaccionPremio.build(params_transaccion).save();
  }
  getRegaliasConfig(codigo_invitacion, forcereject) {
    const { ConfiguracionInstancia, UsuarioModeloInstancia } = this;
    const { ErrorMessages } = this;

    return ConfiguracionInstancia.findOne({
      attributes: [
        "invi_servicio_invitaciones",
        "invi_puntos_x_invitar",
        "invi_puntos_x_aceptar_invitacion",
        "invi_vencimiento",
        "invi_cant_regalias",
        "invi_tiempo_vencimiento"
      ],
      where: { id: 1 }
    })
      .then(configuracion => {
        if (!configuracion) {
          return forcereject == "true" || forcereject === true
            ? Promise.reject("No hay configuracion")
            : { valido: false, error: "No hay configuracion" };
        }
        if (!configuracion.invi_servicio_invitaciones) {
          return forcereject == "true" || forcereject === true
            ? Promise.reject("No tiene habilitado el servicio_invitaciones")
            : {
                valido: false,
                error: "No tiene habilitado el servicio_invitaciones"
              };
        }
        if (
          typeof configuracion.invi_cant_regalias == "undefined" ||
          configuracion.invi_cant_regalias == 0
        ) {
          return forcereject == "true" || forcereject === true
            ? Promise.reject("No tiene cant_regalias_invitaciones definidas")
            : {
                valido: false,
                error: "No tiene cant_regalias_invitaciones definidas"
              };
        }
        if (
          typeof configuracion.invi_puntos_x_invitar == "undefined" &&
          typeof configuracion.invi_puntos_x_aceptar_invitacion == "undefined"
        ) {
          return forcereject == "true" || forcereject === true
            ? Promise.reject("No tiene puntos_x_invitar definidos")
            : { valido: false, error: "No tiene puntos_x_invitar definidos" };
        }
        if (
          typeof configuracion.invi_tiempo_vencimiento == "undefined" &&
          configuracion.invi_vencimiento
        ) {
          return forcereject == "true" || forcereject === true
            ? Promise.reject("No tiene meses asignados para el vencimiento")
            : {
                valido: false,
                error: "No tiene meses asignados para el vencimiento"
              };
        }
        return configuracion;
      })
      .then(configuracion => {
        return UsuarioModeloInstancia.findOne({
          where: { codigo: codigo_invitacion, estado: 1 }
        }).then(usuario => {
          if (!usuario) {
            return forcereject == "true" || forcereject === true
              ? Promise.reject(ErrorMessages["0009"])
              : { valido: false, error: ErrorMessages["0009"] };
          }
          if (typeof configuracion.valido != "undefined") {
            if (configuracion.valido == false) {
              return { valido: false, error: configuracion.error };
            }
          } else {
            return {
              valido: true,
              configuracion: configuracion,
              usuario: usuario
            };
          }
        });
      });
  }
  envioBienvenida(config_cliente, to, usuario, lenguaje_actual = "es") {
    const EmailLangConfig = Config.Lang.email[lenguaje_actual];
    let emailConfig = EmailLangConfig.bienvenida;
    let subject = emailConfig.subject + config_cliente.nombre_empresa;
    return EmailHelper.BuildTemplateLoyalty(
      config_cliente,
      emailConfig.template,
      usuario.id_idioma
    ).then(body => {
      body = GlobalHelper.replaceAll(body, {
        "::nombre_completo::": capitalizeFirstLetter(usuario.nombre),
        "::email::": usuario.email
      });
      return EmailHelper.sendMail(config_cliente, to, subject, body);
    });
  }
  setSaldoCashbackPremios(usuario, configuracion) {
    const { SaldoInstancia } = this;
    let fechaAux = new Date(),
      fechaVencimiento =
        configuracion.acum_vencimiento && configuracion.acum_tiempo_vencimiento
          ? new Date(
              fechaAux.setMonth(
                fechaAux.getMonth() + configuracion.acum_tiempo_vencimiento
              )
            )
          : null,
      puntos_inicio = this.toRound(configuracion.acum_puntos_iniciales);

    if (!(typeof puntos_inicio == "undefined") && puntos_inicio > 0) {
      let params = {
        id_usuario: usuario.id,
        monto: puntos_inicio,
        vencimiento: fechaVencimiento
      };
      return SaldoInstancia.build(params)
        .save()
        .then(() => {
          return usuario
            .increment(["puntos", "puntos_historicos", "puntos_totales"], {
              by: puntos_inicio
            })
            .then(() => {
              return puntos_inicio;
            });
        });
    }
    return Promise.resolve(0);
  }
  //guardar acciones en cola
  async guardarAccionPendiente(alias, data) {
    const { SysColaAcciones } = this;
    const params = {
      alias,
      data: JSON.stringify(data),
      intentos: 0,
      estado: "PENDING",
      completed_at: null
    };
    return SysColaAcciones.build(params).save();
  }
  //validaciones
  async validarSignUp({
    userName,
    email,
    identificacion,
    fecha_nacimiento,
    codigo_preregistro
  }) {
    try {
      const { ErrorMessages, UsuarioModeloInstancia } = this;
      const { validarCodigoPreRegistro } = this;

      const usuarioEmail = await UsuarioModeloInstancia.findOne({
        where: { email }
      });
      const usuarioUsername = await UsuarioModeloInstancia.findOne({
        where: { email: userName }
      });
      const usuarioIdentificacion = await UsuarioModeloInstancia.findOne({
        where: { identificacion }
      });
      const validCodigoPreregitro = await validarCodigoPreRegistro(
        null,
        codigo_preregistro
      );
      const error = { parametros: {} };

      const valido =
        usuarioEmail || usuarioUsername || usuarioIdentificacion ? false : true;

      if (usuarioEmail || usuarioUsername) {
        error.parametros.email = ErrorMessages["0003"];
      }
      if (usuarioIdentificacion) {
        error.parametros.identificacion = ErrorMessages["0001"];
      }
      if (!validCodigoPreregitro.valido) {
        error.parametros.preRegisterCode = ErrorMessages["0011"];
      }
      return { valido, error, status: 400 };
    } catch (error) {
      console.log(error);
      return { valido: false, error: { mensaje: "Error-0003" }, status: 500 };
    }
  }
  async validarCodigoPreRegistro(id_usuario = null, codigo) {
    const { ErrorMessages, PreRegistroInstancia } = this;

    if (!codigo) {
      return { valido: true };
    }
    const cantidad = await PreRegistroInstancia.count({
      where: { id_usuario }
    });
    const data = await PreRegistroInstancia.findOne({
      where: { codigo, estado: 1, id_usuario: { $eq: null } },
      attributes: ["id", "codigo"]
    });

    if (cantidad > 0 && id_usuario) {
      const response = {
        valido: false,
        error: {
          mensaje: ErrorMessages["0012"]
        }
      };
      return response;
    }
    if (!data) {
      const response = {
        valido: false,
        error: {
          mensaje: ErrorMessages["0011"]
        }
      };
      return response;
    }
    if (data.estado == ESTADO_PRE_REGISTRO.deshabilitado) {
      const response = {
        valido: false,
        error: {
          mensaje: ErrorMessages["0011"] //'Sincronizacion deshabilitada'
        }
      };
      return response;
    }
    if (data.estado == ESTADO_PRE_REGISTRO.asociado) {
      const response = {
        valido: false,
        error: {
          mensaje: ErrorMessages["0012"]
        }
      };
      return response;
    }

    return { valido: true, data };
  }
  //Utilidad
  async userAttributesForEndpoint(sub = null) {
    const { UsuarioModeloInstancia, GeneroModeloInstancia } = this;
    const defaultObj = { guest: ["1"], receiveNotifications: ["1"] };
    try {
      if (!sub || sub == "") {
        return defaultObj;
      }

      let usuario = await UsuarioModeloInstancia.findOne({
        where: { sub_cognito: sub, estado: 1 },
        include: [
          {
            required: false,
            model: GeneroModeloInstancia,
            as: "genero",
            attributes: {
              include: [
                [
                  Sequelize.literal(
                    "(SELECT li.traduccion FROM  sys_label_idioma  AS li WHERE li.id_label = genero.id_label_nombre  AND li.id_idioma = 'es' LIMIT 1)"
                  ),
                  "nombre_es"
                ],
                [
                  Sequelize.literal(
                    "(SELECT li.traduccion FROM  sys_label_idioma  AS li WHERE li.id_label = genero.id_label_nombre  AND li.id_idioma = 'en' LIMIT 1)"
                  ),
                  "nombre_en"
                ]
              ]
            }
          }
        ]
      });

      if (!usuario) {
        return defaultObj;
      }
      usuario = usuario.toJSON();

      const cleanValue = (value = null) => {
        return typeof value == "undefined" ||
          value === "null" ||
          typeof value == "object"
          ? ""
          : `${value}`;
      };

      const dateFormat = (date, lang = "es", format = "FULL") => {
        const dateAux = moment(date);
        if (!dateAux.isValid()) {
          return "";
        } else if (format == "MONTH") {
          return dateAux.format("MM");
        } else if (format == "DAY") {
          return dateAux.format("DD");
        } else {
          return lang == "en"
            ? dateAux.format("MM-DD-YYYY")
            : dateAux.format("DD-MM-YYYY");
        }
      };

      const genero_es = usuario.genero ? usuario.genero.nombre_es : "";
      const genero_en = usuario.genero ? usuario.genero.nombre_en : "";

      const UserAttributes = {
        sub: [cleanValue(usuario.sub_cognito)],
        name: [cleanValue(usuario.nombre)],
        lastName: [cleanValue(usuario.apellidos)],
        email: [cleanValue(usuario.email)],
        gender: [cleanValue(usuario.id_genero)],
        gender_name_es: [cleanValue(genero_es)],
        gender_name_en: [cleanValue(genero_en)],
        typeID: [cleanValue(usuario.tipo_identificacion)],
        receiveNotifications: [cleanValue(usuario.acepta_notificaciones)],
        identification: [cleanValue(usuario.identificacion)],
        phone_number: [cleanValue(usuario.telefono)],
        region_code: [cleanValue(usuario.codigo_region)],
        user_code: [cleanValue(usuario.codigo)],
        birthdate: [
          dateFormat(usuario.fecha_nacimiento, usuario.id_idioma, "FULL")
        ],
        birthdate_month: [
          dateFormat(usuario.fecha_nacimiento, usuario.id_idioma, "MONTH")
        ],
        birthdate_day: [
          dateFormat(usuario.fecha_nacimiento, usuario.id_idioma, "DAY")
        ],
        id_idioma: [cleanValue(usuario.id_idioma)]
      };
      return UserAttributes;
    } catch (error) {
      console.log(error);
      return defaultObj;
    }
  }
  toRound(num) {
    // * The Math.round() function returns the value of the given number rounded to the nearest integer.
    const { config_cliente } = this;
    const toRound = !config_cliente
      ? true
      : !config_cliente.PlanConfig.montos_enteros;
    if (isNumber(num)) {
      return toRound ? roundToTwo(parseFloat(num)) : Math.round(num);
    }
    return num;
  }
}

/**
 * [capitalizeFirstLetter description]
 * @param  {[string]} string       []
 */
const capitalizeFirstLetter = string => {
  if (string) return string.charAt(0).toUpperCase() + string.slice(1);
  return "";
};
const mapUserAttributes = async (
  userPoolId = "",
  userObj,
  isSignUp = false
) => {
  //email no es valido
  const validAttributes = [
    "preferred_username",
    "id",
    "nombre",
    "id_genero",
    "apellidos",
    "telefono",
    "fecha_nacimiento",
    "imagen_perfil",
    "id_idioma",
    "identificacion",
    "tipo_identificacion",
    "acepta_notificaciones",
    "fb_user"
  ]; // son los atributos que se quieren y deberian esta en cognito

  if (isSignUp) {
    validAttributes.push("email");
  }

  const alreadyIn = [
    // mmapeo de atributos de lealto a cognito para los atributos que no son custom
    { base: "nombre", cognito: "name" },
    { base: "apellidos", cognito: "family_name" },
    { base: "email", cognito: "email" },
    { base: "telefono", cognito: "phone_number" },
    { base: "fecha_nacimiento", cognito: "birthdate" },
    { base: "imagen_perfil", cognito: "picture" },
    { base: "id_genero", cognito: "gender" },
    { base: "updated_at", cognito: "updated_at" },
    { base: "preferred_username", cognito: "preferred_username" }
  ];
  if (userObj.telefono) {
    // validamos el telefono
    const telefono =
      typeof userObj.telefono == "object"
        ? userObj.telefono.value
        : userObj.telefono;
    userObj.telefono = telefono;
    if (telefono !== "") {
      try {
        const region =
          userObj.codigo_region && userObj.codigo_region !== ""
            ? userObj.codigo_region
            : "CR";
        const number = phoneUtil.parseAndKeepRawInput(
          telefono.toString(),
          region
        );
        userObj.telefono =
          !phoneUtil.isValidNumber(number) ||
          !phoneUtil.isValidNumberForRegion(number, region)
            ? ""
            : phoneUtil.format(number, PNF.E164);
      } catch (error) {
        console.log(error);
        userObj.telefono = "";
      }
    }
  }
  if (userObj.fecha_nacimiento) {
    // fecha como un string
    userObj.fecha_nacimiento = moment(userObj.fecha_nacimiento).format(
      "YYYY-MM-DD"
    );
  }
  if (typeof userObj.id == "number") {
    // intger a string, cognito solo maneja strings
    userObj.id = userObj.id.toString();
  }

  if (
    typeof userObj.identificacion != "undefined" &&
    userObj.identificacion !== null &&
    userObj.identificacion
  ) {
    userObj.preferred_username = userObj.identificacion + "";
  }
  /* mapeo de los atributos con cognito
          id, //custom:id
          nombre,  //name
          id_genero, // genero
          apellidos, //family_name
          email, //email
          telefono, //phone_number
          fecha_nacimiento, //birthdate
          imagen_perfil, //picture
          id_idioma, // custom:idioma 
          identificacion, //custom:identificacion 
          tipo_identificacion, //custom:tipo_identificacion 
          acepta_notificaciones,  //custom:acepta_notificaciones 
          fb_user,  //custom:fb_user 
          fb_id,   //custom:fb_id 
          updated_at //   updated_at
            */
  const keys = Object.keys(userObj);
  const UserAttributes = keys.reduce((result, key, i) => {
    // hace el mapeo de datos a como los espera cognito respecto al esquema anterior
    if (!validAttributes.find((e, i) => e == key)) {
      return result;
    }
    const propertyAux = alreadyIn.find((e, i) => e.base == key);
    const propertyName = propertyAux ? propertyAux.cognito : `custom:${key}`;
    const obj = {
      Name: propertyName,
      Value: userObj[key] ? userObj[key].toString() : ""
    };
    return result.concat(obj);
  }, []);
  const result = await describeUserPool(userPoolId);
  const SchemaAttributes = result.userPool.SchemaAttributes;

  const re = UserAttributes.filter((e, i) =>
    SchemaAttributes.some(
      (a, i) => e.Name == a.Name && (a.Mutable == true || a.Mutable == "true")
    )
  ); // filtarmos todos los atributos que no esten mapeados en cognito para que no reviente el update

  return re;
};
const describeUserPool = async (userPoolId = "") => {
  /* Devuelve un objeto que tiene
    UsernameAttributes — (Array<String>)
      Specifies whether email addresses or phone numbers can be specified as usernames when a user signs up.
    */
  const params = {
    UserPoolId: userPoolId // required
  };

  return cognito
    .describeUserPool(params)
    .promise()
    .then(r => ({ valido: true, userPool: r.UserPool }))
    .catch(err => {
      console.log(err);
      return { valido: false, error: { mensaje: ErrorSystem["500"] } };
    });
};

module.exports = UsuarioController;

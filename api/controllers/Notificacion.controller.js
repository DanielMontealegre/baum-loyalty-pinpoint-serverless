const GlobalHelper = require("../helpers/Global.helper.js");
const EmailHelper = require("../helpers/Email.helper.js");
const Config = require("../../config.js");
const Constants = Config.Constants;
const EmailConfig = Config.Lang.email;
const Promise = require("bluebird");
const minify = require("html-minifier").minify;

const LANG_CODE_MSJ = {
  tipoCodigo: {
    verificacion: {
      en: "verification",
      es: "verificación"
    },
    confirmacion: {
      en: "confirmation",
      es: "confirmación"
    },
    sms: {
      en: (codigo, type, nombre_empresa) =>
        `Your ${type} code is ${codigo}. - ${nombre_empresa}`,
      es: (codigo, type, nombre_empresa) =>
        `Tu código de ${type} es ${codigo}. - ${nombre_empresa}`
    }
  }
};

const createVerificationCustomMessage = async (
  configCliente,
  lang = "es",
  type = "confirmacion",
  user,
  codigo
) => {
  //event.request.codeParameter
  //CustomMessage_SignUp
  const templateConfig =
    type == "confirmacion"
      ? EmailConfig[lang]["CodigoConfirmacion"]
      : EmailConfig[lang]["codigoVerificacion"];

  const template = await EmailHelper.BuildTemplateLoyalty(
    configCliente,
    templateConfig.template,
    lang
  );
  const codeType = LANG_CODE_MSJ["tipoCodigo"][type][lang];

  const body = GlobalHelper.replaceAll(template, {
    "::type_code::": `${codeType}`,
    "::codigo_verificacion::": `${codigo}`,
    "::nombre_completo::": user.name
  });
  const sms = LANG_CODE_MSJ["tipoCodigo"]["sms"][lang](
    codigo,
    codeType,
    configCliente.nombre_empresa
  );

  const bodyMinify = minify(body, {
    caseSensitive: true,
    //collapseInlineTagWhitespace: true,
    removeComments: true,
    collapseWhitespace: true,
    html5: false
  });

  return {
    emailSubject: `${configCliente.nombre_empresa} - ${templateConfig.subject}`,
    emailMessage: bodyMinify,
    smsMessage: sms
  };
};

module.exports.createVerificationCustomMessage = createVerificationCustomMessage;

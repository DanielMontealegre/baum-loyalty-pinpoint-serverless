// AWS-SDK
const AWS = require('aws-sdk');
const SSM = new AWS.SSM();
const cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider({ apiVersion: '2016-04-18' });

//utility
const ConfigGeneral = require('../../config.js');


const getParametersByPathAll = async(NextToken, result = []) => {
    const params = {
        Path: process.env.SSM_PATH,
        WithDecryption: true,
        NextToken      
    };

    const envVariable = await SSM.getParametersByPath(params)
        .promise()
        .catch(error => {
            console.log(`Error SSM.getParametersByPath  => ${error}`);
            return { Parameters: []};
        });

    if(!envVariable.NextToken){
        return result.concat(envVariable.Parameters);
    }
    else{
        return await getParametersByPathAll(envVariable.NextToken, result.concat(envVariable.Parameters));
    }
};
const getEnviromentVariablesFromSSM = async() => {
    try {
        // database config
        const database_host_write = process.env.PATH_DB_WRITE_HOST;
        const database_host_read = process.env.PATH_DB_READ_HOST;
        const database_schema = process.env.PATH_DB_NAME;
        const database_user = process.env.PATH_DB_USER;
        const database_password = process.env.PATH_DB_PASS;
        const database_port = process.env.PATH_DB_PORT;


        const envVariable = await getParametersByPathAll(null,[]);
        const port = getValueFromArray(envVariable, database_port);

        //db
        const databaseConfig = {
            nombre: getValueFromArray(envVariable, database_schema),
            usuario: getValueFromArray(envVariable, database_user),
            password: getValueFromArray(envVariable, database_password).trim(),
            puerto: ((port == '') ? 3306 : port),
            host_lectura: getValueFromArray(envVariable, database_host_read),
            host_escritura: getValueFromArray(envVariable, database_host_write)
        };

        const finalResult = {
            databaseConfig,
            valid: ((envVariable.length == 0) ? false : true)
        }
        return finalResult;
    } catch (error) {
        console.log(`Error getEnviromentVariablesFromSSM => ${error}`);
        return {};
    }
};
const getEnviromentVariablesFromEnv = () => {
    const databaseConfig = {
        nombre: process.env.DB_NAME,
        usuario: process.env.DB_USER,
        password: process.env.DB_PASS,
        puerto: process.env.DB_PORT,
        host_lectura: process.env.DB_READ_HOST,
        host_escritura: process.env.DB_WRITE_HOST
    };

    return { databaseConfig, valid: true };
};
const getDBConfig = async() => {
    try {
        return await getEnviromentVariablesFromSSM();
    } catch (error) {
        console.log(`Error getDBConfig ${error} =>`);
        return false;
    }
};

//utility
const getValueFromArray = (array, name) => {
    const index = array.findIndex(e => e.Name == name);

    return (index < 0) ? '' : array[index].Value.toString().trim();
};

module.exports = {
    getEnviromentVariablesFromSSM,
    getEnviromentVariablesFromEnv,
    getDBConfig
}
//Packages
const FS = require("fs");
const moment = require("moment-timezone");
const Sequelize = require("sequelize");
const R = require("ramda");
const util = require("util");
const Promise = require("bluebird");
const concat = require("concat-files");
//Sequelize ini de cliente
const sequelizeInit = require("../core/Sequelize.js");

//Helpers
const GlobalHelper = require("../helpers/Global.helper.js");

//Services
const SESAWSService = require("../services/SESAWS.service.js");
const AWS = require("aws-sdk");
const pinpoint = new AWS.Pinpoint({});
//config
const Config = require("../../config.js");
const Constants = Config.Constants;
const S3Config = Config.Storage.s3_aws_config; // es la config del bucket de master
const EmailService = Config.Email.current_email_service;

const templatesMemory = {};

//Models cliente
const EmailSendErrorArch = require("../models/EmailSendError.model.js");

/*
 * Función para realizar envíos por correo
 * parametros:
 *  to-> Array of objects: [{email: ''{, name: ''}}, {email: ''{, name: ''}}, ...]
 *  subject-> Text
 *  body-> HTML Text
 *  from-> Object: {email: ''{, name: ''}}
 */
module.exports.sendMail = async (config_cliente, to, subject, body, from) => {
  try {
    let sequelize = sequelizeInit.init(
      R.prop("DataBaseConfig", config_cliente)
    );
    EmailSendError = EmailSendErrorArch.init(sequelize);

    const isAWSAnalytics = R.path(
      ["AWSConfig", "aws_analytics"],
      config_cliente
    );
    const ApplicationId = R.path(
      ["AWSConfig", "pinpoint_application_id"],
      config_cliente
    );
    const arn_role_aws_analytics = R.path(
      ["AWSConfig", "arn_role_aws_analytics"],
      config_cliente
    );
    const service = isAWSAnalytics ? "pinpoint" : EmailService;
    switch (service) {
      case "ses_aws":
        console.log("SES - Correos " + JSON.stringify(to) + " " + subject);
        SESAWSService.sendMail(
          config_cliente,
          to,
          subject,
          body,
          from,
          function(result, error, params) {
            if (error) {
              console.log(error);
              guardarLogEmail(EmailSendError, params, error.message, "ses_aws");
            } else {
              console.log(result);
            }
          }
        );
        break;
      case "pinpoint":
        console.log("PINPOINT - Correos " + JSON.stringify(to) + " " + subject);
        const toAddress = (ac, x) =>
          R.merge({ [R.prop("email", x)]: { ChannelType: "EMAIL" } }, ac);

        const fromPlus = R.path(
          ["AWSConfig", "ses_default_from_email"],
          config_cliente
        );
        const fromName = R.path(
          ["AWSConfig", "ses_default_from_name"],
          config_cliente
        );
        const FromAddress =
          !R.isNil(fromName) && !R.isEmpty(fromName)
            ? `${fromName} <${fromPlus}>`
            : fromPlus;
        const Addresses = R.reduce(toAddress, {}, to);
        const Context = {};
        const TraceId = "";
        const MessageConfiguration = {
          EmailMessage: {
            Body: body,
            FromAddress,
            SimpleEmail: {
              HtmlPart: {
                Charset: "UTF-8",
                Data: body
              },
              Subject: {
                Charset: "UTF-8",
                Data: subject
              }
            }
          }
        };

        const params = {
          ApplicationId,
          MessageRequest: {
            Addresses,
            Context,
            Endpoints: {},
            MessageConfiguration
          }
        };
        console.log("Sending Message!");
        const result = await pinpoint
          .sendMessages(params)
          .promise()
          .then(r => (r ? { ...r, valido: true } : { valido: false }))
          .catch(err => {
            console.log(`Error in sendMessages ${err}`);
            return { valido: false, error: err };
          });

        console.log("Message send!");
        console.log({ Result: JSON.stringify(result) });

        if (!result.valido) {
          guardarLogEmail(
            EmailSendError,
            MessageConfiguration,
            result.error.message,
            "pinpoint"
          );
        }
        break;
      default:
        console.log("Servicio de envíos inválido");
        break;
    }
  } catch (error) {
    console.log(error);
    return;
  }
};

const guardarLogEmail = (EmailSendError, params, error_message, service) => {
  if (!EmailSendError) {
    return;
  }
  let saveParams = {
    servicio: service, //Cambiar
    detalle: error_message,
    parametros:
      process.env.NODE_ENV == "dev" ? "NODE_ENV = DEV" : JSON.stringify(params),
    created_at: new Date().getTime(),
    ultimo_intento: new Date().getTime(),
    intentos: 0
  };

  return EmailSendError.build(saveParams)
    .save()
    .then(log => {})
    .catch(error => {
      console.log(error);
    });
};

/**
 * [loadBodyFromFile description]
 * @param  {[type]} template [description]
 * @param  {[type]} callBack [description]
 * @return {[type]}          [description]
 */
const loadBodyFromFile = async (template, callBack = () => {}) => {
  const readFile = Promise.promisify(FS.readFile);
  const file = await readFile(template, "utf8").catch(error => {
    console.log(`readFile =======> ${error}`);
    return "";
  });
  callBack(file);
  return file;
};

module.exports.loadBodyFromFile = loadBodyFromFile;

/**
 * [BuildTemplateLoyalty description]
 * @param  {[object]} config_cliente [configuracion del cliente]
 * @param  {[string]} template [description]
 * @param  {[string]} idioma [en,es]
 * @param  {[string]} concatHeader [Si concatenar el header]
 * @param  {[string]} concatFooter [Si concatenar el footer]
 * @return {[string]}          [description]
 */
const BuildTemplateLoyalty = async (
  config_cliente,
  template,
  idioma = "es",
  concatHeader = true,
  concatFooter = true
) => {
  let s3_url = config_cliente.AWSConfig.s3_url
    ? config_cliente.AWSConfig.s3_url
    : "no_url/";

  let sequelizeInstance = sequelizeInit.init(config_cliente.DataBaseConfig);

  const configSocial = await sequelizeInstance.query(
    "SELECT id, id_configuracion, nombre, url, icono FROM `pl_config_social` where id_configuracion = ?",
    {
      replacements: [Constants.tablaConfiguraciones.id],
      type: Sequelize.QueryTypes.SELECT
    }
  );

  let nombre_empresa = config_cliente.nombre_empresa;
  const configCorreo = config_cliente.CorreoConfig;
  const configApariencia = config_cliente.AparienciaConfig;

  const color_primario =
      configApariencia && configApariencia.color_primary
        ? configApariencia.color_primary
        : "#000000",
    color_secundario =
      configApariencia && configApariencia.color_secondary
        ? configApariencia.color_secondary
        : "#000000";

  let tabla_contenido_social = "";
  if (configSocial) {
    tabla_contenido_social = configSocial.reduce(
      (accumulator, currentValue) => {
        let a =
          '<a href="' +
          currentValue.url +
          '" target="_blank" title="' +
          currentValue.nombre +
          '" style="display: inline-block; margin-left: 5px;">' +
          '<img style="display:block; line-height:0px; font-size:0px; border:0px; width: 24px; height: 24px;" src="' +
          S3Config.url +
          currentValue.icono +
          '" alt="' +
          currentValue.nombre +
          '"/>' +
          "</a>";
        return accumulator + a;
      },
      ""
    );
  }

  let img_logo =
    configCorreo.banner === null ? "" : s3_url + configCorreo.banner;

  let moment_now = new moment();

  let alas =
    moment_now.hour() == 13 || moment_now.hour() == 1 ? "a la" : "a las";

  let fecha_actual =
    idioma == "en"
      ? `on the ${moment_now.format("DD/MM/YYYY")} at ${moment_now.format(
          "hh:mm A"
        )}`
      : `el ${moment_now.format("DD/MM/YYYY")} ${alas} ${moment_now.format(
          "hh:mm A"
        )}`;

  let VaraiblesComunes = {
    "::fecha_actual::": fecha_actual,
    "::img_logo::": img_logo,
    "::nombre_empresa::": " " + nombre_empresa,
    "::color_base::": color_primario,
    "::color_secundario::": color_secundario,
    "::url_visita::": configCorreo.url_site,
    "::display_type::":
      typeof configCorreo.url_site == "undefined" ? "none" : "block",
    "::url_appstore::": configCorreo.url_playstore,
    "::url_playstore::": configCorreo.url_appstore,
    "::img_playstore::": S3Config.url + Constants.stores_app.appstore,
    "::img_appstore::": S3Config.url + Constants.stores_app.googleplay,
    "::tabla_redes_sociales::": tabla_contenido_social,
    "::pagina_lealto::": Constants.stores_app.lealtoPagina,
    "::pagina_lealto_www::": Constants.stores_app.lealtoPaginaWWW,
    "::logo_lealto::": S3Config.url + Constants.stores_app.lealtoLogo,
    "::year::": moment_now.format("YYYY"),
    "::font_family::": "Montserrat, Arial, sans-serif;",
    "::font_Family::": "Montserrat, Arial, sans-serif;" // Open Sans', Arial, 'sans-serif
  };
  //Montserrat', Arial, 'sans-serif

  const returnFile = templateResult => {
    let final = GlobalHelper.replaceAll(templateResult, VaraiblesComunes);
    return final;
  };
  const templateResult = await buildEmailTemplateFromMapping(
    template,
    idioma,
    concatHeader,
    concatFooter
  );
  return returnFile(templateResult);
};
/**
 * [buildEmailTemplateFromMapping description]
 * @param  {[string]} template [description]
 * @param  {[string]} lang [en,es]
 * @param  {[string]} concatHeader [Si concatenar el header]
 * @param  {[string]} concatFooter [Si concatenar el footer]
 * @return {[string]}          [description]
 */
const buildEmailTemplateFromMapping = async (
  template,
  lang,
  concatHeader = true,
  concatFooter = true
) => {
  try {
    //archivo de la plantilla final generada

    let tpl =
      Constants.root +
      "files/email-templates/final/" +
      lang +
      "/" +
      template +
      ".html";

    if (templatesMemory[tpl]) {
      const aux = templatesMemory[tpl];
      return aux + "";
    }

    //mapeo de la plantilla
    let mapping = require(Constants.root +
      "files/email-templates/mapping-general.js");

    if (mapping) {
      mapping = mapping[lang];
      //content
      let contentHtml = await loadFile(
        Constants.root +
          "files/email-templates/" +
          mapping.content +
          "/" +
          template +
          ".html"
      );
      let headerHtml = "";
      let footerHtml = "";
      //header
      if (concatHeader) {
        headerHtml = await loadFile(
          Constants.root + "files/email-templates/" + mapping.header
        );
      }
      //footer
      if (concatFooter) {
        footerHtml = await loadFile(
          Constants.root + "files/email-templates/" + mapping.footer
        );
      }
      const html = "".concat(headerHtml, contentHtml, footerHtml);
      templatesMemory[tpl] = html;
      return html;
    }
    return "";
  } catch (error) {
    logger.alert(error);
    return "Could not concat this template: " + template;
  }
};

/**
 * [loadBodyFromFile description]
 * @param  {[type]} template [description]
 * @param  {[type]} callBack [description]
 * @return {[type]}          [description]
 */
const loadFile = async (fileParam, callBack = () => {}) => {
  const readFile = Promise.promisify(FS.readFile);
  const file = await readFile(fileParam, "utf8").catch(error => {
    console.log(`readFile =======> ${error}`);
    return "";
  });
  callBack(file);
  return file;
};

module.exports.BuildTemplateLoyalty = BuildTemplateLoyalty;

//Packages
const cache = require('memory-cache');
const fs = require('fs');
const path = require('path');
const Promise = require('bluebird');

//Sequelize init de cliente y instancia master
const sequelizeToInit = require('../core/Sequelize.js');
const Sequelize = require('sequelize');
//Config.
const Config = require('../../config.js');
const Constants = Config.Constants;
const config_clientes = 'configCachceClientes';

const CACHE_USAGE = (process.env.cache_usage)? true : false;

const cacheName = (id_cliente) => {

    return `${config_clientes}-${id_cliente}`;
}


const getClienteConfig = (id_cliente,conexionDbMaster) => {

    return getClienteConfigFromCache(id_cliente,conexionDbMaster);
}

const getClienteConfigFromCache = (id_cliente,conexionDbMaster) => {
    let config_clientes_json = cache.get(cacheName(id_cliente));
    if (config_clientes_json && CACHE_USAGE) {
        return Promise.resolve(config_clientes_json);
    } else {
        console.log("No habia cache!");
        return getClienteConfigFromDB(id_cliente,conexionDbMaster); //getClienteConfigFromFile(id_cliente) con file //updateConfigCacheFromDB()
    }
}

const getClienteConfigFromDB = (id_cliente,conexionDbMaster) => {
    return getConfigClienteV2(id_cliente,conexionDbMaster).then(r => {
        if(r){
            updateCacheFromJSON(r,id_cliente);
            return r;
        }
        else{
            return null;
        }
    })
}

const updateCacheFromJSON = (json,id_cliente) => {
    cache.del(cacheName(id_cliente));
    cache.put(cacheName(id_cliente), json);
    return Promise.resolve(true);
}


///**************************************************//


const getClienteFromEvent = (event,conexionDbMaster) => {
    //revisar el id del pool y aque cliente esta asociada en la base master
    //devolver el id del cliente
    const userPoolId = event.userPoolId;
    return conexionDbMaster.query("SELECT cliente.id, nombre FROM `cliente` inner join config_aws on cliente.id_config_aws = config_aws.id where config_aws.cognito_user_pool_id = ? limit 1", { replacements: [userPoolId], type: Sequelize.QueryTypes.SELECT })
    .then(r => (r[0])? r[0].id : null)
    .catch(err => {console.log(err); return null});
};
const getClienteFromEventPinpoint = (event,conexionDbMaster) => {
    //revisar el id de la aplicacion de pinpoint y aque cliente esta asociada en la base master
    //devolver el id del cliente
    const applicationId = event.ApplicationId;
    return conexionDbMaster.query("SELECT cliente.id, nombre FROM `cliente` inner join config_aws on cliente.id_config_aws = config_aws.id where config_aws.pinpoint_application_id = ? limit 1", { replacements: [applicationId], type: Sequelize.QueryTypes.SELECT })
    .then(r => (r[0])? r[0].id : null)
    .catch(err => {console.log(err); return null});
};
const getConfigClienteV2 = (id_cliente,conexionDbMaster) => {
    return conexionDbMaster.query("SELECT id, nombre,id_plan,id_config_db, id_config_aws, id_config_apariencia, id_config_correo, username, estado, msj_estado FROM `cliente` where id = ?", { replacements: [id_cliente], type: Sequelize.QueryTypes.SELECT })
        .then(clientes => { return (clientes) ? clientes[0] : {} }) //Tirar error aqui o algo en vez de un array vacio
        .then(cliente => {
            return conexionDbMaster.query("SELECT id, nombre, usuario, password, host_escritura, host_lectura, puerto FROM `config_db` where id = ?", { replacements: [cliente.id_config_db], type: Sequelize.QueryTypes.SELECT })
                .then(config_db => { cliente.id_config_db = config_db[0]; return cliente; })
        })
        .then(cliente => {
            return conexionDbMaster.query("SELECT id, banner,url_site, url_appstore, url_playstore FROM `config_correo` where id = ?", { replacements: [cliente.id_config_correo], type: Sequelize.QueryTypes.SELECT })
                .then(config_correo => { cliente.id_config_correo = config_correo[0]; return cliente; })
        })
        .then(cliente => {
            return conexionDbMaster.query(`SELECT 
                id, 
                nombre, 
                tipo, 
                niveles,version,
                montos_enteros, 
                niveles as pl_niveles,
                cupones as pl_servicio_cupones,
                encuestas as pl_servicio_encuestas,
                anuncios as pl_servicio_anuncios,
                acum_compra_domicilio as pl_servicio_compra_domicilio,
                regalias as pl_servicio_regalias,
                nombre as pl_nombre,
                tipo as pl_tipo,
                acum_regalia,
                acum_default,
                acum_compra_domicilio,
                estado FROM \`plan_lealtad\` where id = ?`, { replacements: [cliente.id_plan], type: Sequelize.QueryTypes.SELECT })
                .then(plan_lealtad => { cliente.id_plan = plan_lealtad[0]; return cliente; })
        })
        .then(cliente => {
            return conexionDbMaster.query("SELECT id, ses_default_from_email, ses_default_from_name, s3_bucket, s3_url, sns_ios_arn, sns_android_arn, cognito_user_pool_id as user_pool_id, cognito_migration,  pinpoint_application_id, aws_analytics_arn_role as arn_role_aws_analytics, aws_analytics, pinpoint_default_from_email FROM `config_aws` where id = ?", { replacements: [cliente.id_config_aws], type: Sequelize.QueryTypes.SELECT })
                .then(config_aws => { return config_aws[0]; })
                .then(config_aws => {
                    return conexionDbMaster.query("SELECT id, id_config_aws, id_idioma, sns_topic_arn, sns_guest_topic_arn FROM `config_aws_sns` where id_config_aws = ?", { replacements: [config_aws.id], type: Sequelize.QueryTypes.SELECT })
                        .then(config_aws_sns => {
                            let auxConfigAWS = {
                                "config_aws": config_aws,
                                "config_aws_sns": config_aws_sns
                            }
                            cliente.id_config_aws = auxConfigAWS;

                            return cliente;
                        })
                })
        })
        .then(cliente => { return (cliente) ? cliente : null; })
        .catch(err => { console.log(err) });
};
const createGlobalObjConfig = (config_cliente) => {



    let config_clienteObj = {
        id_cliente: config_cliente.id,
        DataBaseConfig: config_cliente.id_config_db,
        AWSConfig: config_cliente.id_config_aws.config_aws,
        CorreoConfig: config_cliente.id_config_correo,
        AWSSNSConfig: config_cliente.id_config_aws.config_aws_sns,
        PlanConfig: config_cliente.id_plan,
        Plan_Cliente :   config_cliente.id_plan,
        nombre_empresa: config_cliente.nombre,
        id_cliente : config_cliente.id
    }

    global.DataBaseConfig = config_clienteObj.DataBaseConfig;
    global.AWSConfig = config_clienteObj.AWSConfig;
    global.CorreoConfig = config_clienteObj.CorreoConfig;
    global.Plan_Cliente = config_clienteObj.PlanConfig;





    // lo siguiente es para darle el mismo formata que tiene los arns en el archivo de configuracion de notificaciones 
    let arns = config_clienteObj.AWSSNSConfig.reduce((acumulador, e, i) => {
        let idioma_arns = {
            topic_arn: e.sns_topic_arn,
            guest_topic_arn: e.sns_guest_topic_arn,
            ios_arn: global.AWSConfig.sns_ios_arn,
            android_arn: global.AWSConfig.sns_android_arn
        }

        acumulador[e.id_idioma] = idioma_arns
        return acumulador;
    }, {})

    let AWSSNSConfig = {
        arns: arns

    }
    global.AWSSNSConfig = AWSSNSConfig;

    global.config_cliente = config_clienteObj;

    config_clienteObj.AWSSNSConfig = AWSSNSConfig;
    config_clienteObj.DataBaseConfig.montos_enteros = config_clienteObj.PlanConfig.montos_enteros;

    return config_clienteObj
};

module.exports.getConfigCliente = async (event,dbConfig = null, isPinpointEvent = false) => {
  if(!dbConfig){
    return { valido: false ,status:500, error: { mensaje: ('Configuracion de DB invalida - 0000') }};
  }
  const conexionDbMaster = sequelizeToInit.init(dbConfig);

  const validConexion = await conexionDbMaster.authenticate();

  const id_cliente = isPinpointEvent ? await getClienteFromEventPinpoint(event,conexionDbMaster) : await getClienteFromEvent(event,conexionDbMaster);
  if (!id_cliente) {
    return { valido: false ,status:500, error: { mensaje: ('Configuracion de cliente invalido - 0001') }};
  }
  return getClienteConfig(id_cliente,conexionDbMaster)
    .then(config => {
      if (config == null || !config) {
            return { valido: false , status:500, error: { mensaje: ('Configuracion de cliente invalido - 0002 ') }};
      }
      let config_cliente = createGlobalObjConfig(config);

      let sequelizeInstance = sequelizeToInit.init(config_cliente.DataBaseConfig);
      return sequelizeInstance.authenticate()
        .then(() => {
          console.log('Connection has been established successfully.');
          return { valido: true, config_cliente, sequelizeCliente: sequelizeInstance };
        })
        .catch(error => {
          console.error('Unable to connect to the database:', error);
           return { valido: false ,status:500, error: { mensaje: ('Error inesperado - Unable to connect to the database') }};
        })
    })
    .catch((error) => {
      console.log(error);
      return { valido: false ,status:500, error: { mensaje: ('Error inesperado - 0001 ') }};
    })
}


module.exports.getClienteFromEvent = getClienteFromEvent;
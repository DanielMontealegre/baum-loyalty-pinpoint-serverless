
const FS = require('fs');
const moment = require('moment-timezone');
const Sequelize = require('sequelize');
const Promise = require('bluebird');
const currencyFormatter = require('currency-formatter');

//Helpers
const EmailHelper = require('./Email.helper.js');
//Services
const S3 = require('../services/S3AWS.service.js');

//Models cliente
const MedioNotifacionModel = require('../models/MedioNotificacion.model.js');
const SysLabelModel = require('../models/SysLabel.model.js');
const NivelModel = require('../models/Nivel.model.js');
const UsuarioModel = require('../models/Usuario.model.js');
const ConfiguracionModel = require('../models/Configuracion.model.js');
const TransaccionNivelModel = require('../models/TransaccionNivel.model.js');


//Config
const Config = require('../../config.js');
const Constants = Config.Constants;
const S3AWSConfig = Config.Storage.s3_aws_config;
const LangConfig = Config.Lang;
const MONEY_FORMAT = Config.MoneyFormat;
const medioNotifacionAlias = Constants.MedioNotifacionAliases;


/*
 * Función rellenar string con un caracter
 * parametros:
 *  str-> String al que se le aplica el relleno
 *  char-> Caracter con el que se rellena el string
 *  length-> Tamaño que debe tener el string con el relleno
 */
module.exports.pad = (str, char, length) => {
  let padding = Array(length).join(char);
  return (padding + str).slice(-length);
};

/*
 * Función para traer el dia de la semana de la fecha dada con o sin iso
 * parametros:
 *  useISO-> boolean
 *  useISO-> moment o date
 * return userIso => true => [1,2,3,4,5,6,7] || userIso => false => [0.1,2,3,4,5,6]
 */
module.exports.isoWeekdayFunc = (useISO = true,fecha_actualParam = moment()) => {
  const fecha_actual = moment(fecha_actualParam);
  if(useISO){
    return fecha_actual.isoWeekday();
  }else{
    const dia = fecha_actual.isoWeekday();
    return (dia == 7)? 0 : dia;
  }
}



const image_typeFunc = (img_base64) => {
  if (img_base64.indexOf('data:image/png') >= 0) {

    img_base64 = img_base64.replace('data:image/png;base64,', '');

    return { type: 'png', img_base64: img_base64 };

  } else if (img_base64.indexOf('data:image/jpeg') >= 0) {

    img_base64 = img_base64.replace('data:image/jpeg;base64,', '');

    return { type: 'jpg', img_base64: img_base64 };

  } else if (img_base64.indexOf('data:image/jpg') >= 0) {

    img_base64 = img_base64.replace('data:image/jpg;base64,', '');

    return { type: 'jpg', img_base64: img_base64 };

  } else if (img_base64.indexOf('data:image/gif') >= 0) {
    img_base64 = img_base64.replace('data:image/gif;base64,', '');

    return { type: 'gif', img_base64: img_base64 };

  } else {
    return { type: '', img_base64: '' };
  }
}

/*
 * Función convertir una imagen base64 a un archivo con url y lo guarda en el S3
 */
const imageBase64toUrlS3 = async(bucket, img_base64, file_name = '',bucketDir) => {
  //si no vieneo es vacio

  try {
    if (!img_base64 || img_base64 === '') {
      return '';
    }

    //si ya hay una imagen
    if (img_base64.indexOf('https') != -1) {
      return img_base64;
    }

    //se obtiene tipo de imagen, y el data base64 limpio
    let image_type;
    let result = image_typeFunc(img_base64);

    if (result.type == '') {
      return '';
    }

    image_type = result.type;
    img_base64 = result.img_base64;

    bucket = bucket + '/' +bucketDir;
    const Body = new Buffer(img_base64, 'base64');
    const file_name_full =  `${file_name}.${image_type}`;
    //guardamos la imagen en el s3
    const resultS3 = await S3.putObject(bucket,file_name_full, Body, 'public-read',`image/${image_type}`,'base64').catch(error => { console.log(error); return false});

    return (resultS3)? (bucketDir + "/" + file_name_full) : '';

  } catch (error) {
    console.log(error);
    return '';
  }
};

/*
 * Función convertir una imagen base64 a un archivo con url
 */
module.exports.imageBase64toUrl = (img_base64, id_cliente, callBack) => {
  if (!img_base64 || img_base64 === '') {
    callBack('');
    return;
  }
  let dir = './files/profile-images/',
    url = 'http://www.premflight.com/files/profile-images/';
  //  url = Constants.url + 'files/profile-images/';
  //se obtiene tipo de imagen, y el data base64 limpio

  let image_type;
  let result = image_typeFunc(img_base64);

  if (result.type == '') {
    callBack(''); // si no es alguno de estos tipos
    return;
  }

  image_type = result.type;
  img_base64 = result.img_base64;

  let image_data = new Buffer(img_base64, 'base64');
  //generamos un nombre de archivo
  let file_name = 'image-user-' + id_cliente + '_' + new Date().getTime() + '.' + image_type;
  //guardamos la imagen en el archivo
  try {
    FS.writeFile(dir + file_name, image_data, (err) => {
      if (err) {
        console.log(err);
        callBack('');
        return;
      }
      callBack(url + file_name);
      return;
    });
  } catch (error) {
    callBack('');
    return;
  }
};
/*
 * Función convertir una imagen base64 a un archivo con url y lo guarda en el S3
 */
module.exports.imageUsuarioBase64toUrlS3 = (config_cliente, img_base64, isQr, ident_usuario, ident_cliente, callBack) => {
  //si no vieneo es vacio
  if (!img_base64 || img_base64 === '') {
    callBack('');
    return;
  }

  //si ya hay una imagen
  if (img_base64.indexOf('https') != -1) {
    callBack(img_base64);
    return;
  }


  //se obtiene tipo de imagen, y el data base64 limpio
  let image_type;
  let result = image_typeFunc(img_base64);

  if (result.type == '') {
    callBack(''); // si no es alguno de estos tipos
    return;
  }

  image_type = result.type;
  img_base64 = result.img_base64;
  //generamos un nombre de archivo
  let file_name = ((isQr) ? 'qr-' : 'image-user-') + ident_usuario + '_' + new Date().getTime() + '.' + image_type;
  //guardamos la imagen en el s3
  S3.saveImage(config_cliente, img_base64, file_name, 'image/' + image_type, ident_usuario, ident_cliente, function(err) {
    if (err) {
      callBack('');
      return;
    }
    let nombre_empresa = config_cliente.nombre_empresa;
    let direccion = "usuarios/" + "usuario" + "-" + ident_usuario + "/imagenes-perfil";
    callBack(direccion + "/" + file_name);
  });
};

module.exports.imageCodigoCuponBase64toUrls3 = (config_cliente = {},img_base64,file_name) => {
    const bucketDir = `cupones/codigos-qr`;
    const bucket = (config_cliente.hasOwnProperty('AWSConfig'))? config_cliente.AWSConfig.s3_bucket : '';
    return imageBase64toUrlS3(bucket, img_base64, file_name, bucketDir);
}



/*
 * Función para eliminar un archivo
 * @param {String} file_dir
 * @returns {undefined}
 */
module.exports.deleteFile = (file_dir) => {
  FS.exists(file_dir, (exists) => {
    if (exists) {
      FS.unlinkSync(file_dir);
    }
  });
};


/*
 * Función para eliminar un archivo del S3
 * @param {String} file_dir
 * @returns {undefined}
 */
module.exports.deleteFileS3 = (config_cliente, file_dir) => {
  let file = file_dir.replace(config_cliente, "");
  S3.deleteFile(file);
};

/*
 * Función para remplazar carateres en un string
 * @param {String} str
 * @param {Object} mapObj ({search: replace})
 * @returns {String}
 */
const replaceAll = (str, mapObj) => {
  let re = new RegExp(Object.keys(mapObj).join("|"), "gi");

  const aux = str.replace(re, (matched) => {
    return mapObj[matched.toLowerCase()];
  });
  return aux;
}

module.exports.replaceAll = (str, mapObj) => {
  return replaceAll(str, mapObj);
};

/**
 * [DateFormat description]
 * @param {[type]} date [description]
 */
module.exports.DateFormat = (d) => {
  try {
    let dia = d.getDate();

    if (dia < 10) {
      dia = "0" + dia;
    }
    let mes = d.getMonth() + 1;

    if (mes < 10) {
      mes = "0" + mes;
    }

    let hora = d.getHours();
    if (hora < 10) {
      hora = "0" + hora;
    }

    let minutos = d.getMinutes();
    if (minutos < 10) {
      minutos = "0" + minutos;
    }

    let segundos = d.getSeconds();

    if (segundos < 10) {
      segundos = "0" + segundos;
    }
    return d.getFullYear() + "-" + mes + "-" + dia + " " +
      hora + ":" + minutos + ":" + segundos;
  } catch (err) {
    console.log(err);
  }
};

/**
 * [FormatMoney description]
 * @param {[type]} amount [description]
 */
module.exports.FormatMoney = (amount) => {
  let a = typeof amount !== 'undefined' && amount !== null ? amount : 0;
  return 'USD $' + a.toFixed(2);
}


/**
 * [RandomString description]
 * @param {[type]} len [description]
 */
module.exports.RandomString = (len) => {
  let text = " ";
  let charset = "abcdefghijklmnopqrstuvwxyz0123456789";
  for (let i = 0; i < len; i++)
    text += charset.charAt(Math.floor(Math.random() * charset.length));
  return text;
};

/**
 * [EvaluateMode description]
 * @param {[type]}   req  [description]
 * @param {[type]}   res  [description]
 * @param {Function} next [description]
 */
module.exports.EvaluateMode = (req, res, next) => {
  let url = req.baseUrl;
  if (process.env.NODE_ENV == 'production' && url.indexOf('test') != -1) {
    process.env.NODE_ENV = 'development';
  }
  next();
};



let sys_label_idioma = Constants.sys_label_idioma;


/**
 * [getLabelFromString description]
 * @param {[string]}   idioma  [en,es,fr]
 * @param {[string]}   traduccion  ["something something"]
 */
const getLabelFromString = (sequelize, idioma, syslabelId, traduccion) => {
  if (!sequelize) {
    return Promise.reject("No database connection");
  }
  return sequelize.query('INSERT INTO ' + sys_label_idioma + ' (id_label, id_idioma, traduccion) values (?,?,?)', { replacements: [syslabelId, idioma, traduccion], type: Sequelize.QueryTypes.INSERT })
}

const insertSyslabel = (sequelize) => {
  if (!sequelize) {
    return Promise.reject()
  }
  let SysLabel = SysLabelModel.init(sequelize);
  return SysLabel.build().save().then(s => { return s; }).catch(error => {
    console.log(error);
    return;
  });
}

module.exports.insertSyslabel = insertSyslabel;



const envioNotifacionEmail = async(sequelize, config_cliente, usuario, datos, alias) => {


  if (!datos) {
    return;
  }
  try {
    const Configuracion = ConfiguracionModel.init(sequelize);

    let idioma_usuario = usuario.id_idioma;
    let to = [{ email: usuario.email, name: usuario.nombre }],
      EmailLangConfig = Config.Lang.email[idioma_usuario],
      emailConfig = EmailLangConfig[alias],
      subject = emailConfig.subject;

    let template;

    let sucursal_nombre = (datos.sucursal) ?
      ((idioma_usuario == 'es') ?
        `en nuestra tienda ${datos.sucursal.nombre}`

        :
        `at ${datos.sucursal.nombre}`) :
      '';

    let null_replacement = (idioma_usuario == 'es') ? 'no disponible' : 'not available';

    const moneyFormat = await Configuracion.findOne({
        where: { id: Constants.tablaConfiguraciones.id },
        attributes: ['id', 'pl_formato_montos']
      })
      .then(result => ((result) ? (MONEY_FORMAT[((typeof result.pl_formato_montos == 'undefined' || result.pl_formato_montos === null) ? 1 : result.pl_formato_montos)]) : MONEY_FORMAT[1]))
      .catch(error => MONEY_FORMAT[1]);

      moneyFormat.symbol = ''; // para prevenir el doble simbolo. Deberia pasarse todo a moneyFormat con symbolo

    if (alias == medioNotifacionAlias.acumular) {
      template = (Constants.tipoDePlan.sellos == config_cliente.Plan_Cliente.tipo) ? '-sellos' : '-cashbackYpremios';
      template = (datos.NivelNuevo) ? template + "-nivel" : template;



      EmailHelper.BuildTemplateLoyalty(config_cliente, emailConfig.template + template, idioma_usuario, (body) => {
        body = replaceAll(body, {
          '::sucursal_nombre::': sucursal_nombre,
          '::nombre_completo::': datos.nombre_usuario,
          '::nombre_moneda::': datos.nombre_moneda,
          '::puntos_disponibles::': currencyFormatter.format(datos.puntos_disponibles, moneyFormat),
          '::puntos_acumulados::': currencyFormatter.format(datos.puntos_acumulados, moneyFormat),
          '::title::': datos.titulo,
          '::premios_transaccion::': datos.premios_transaccion,
          '::premios_disponibles::': datos.premios_disponibles,
          '::simbolo_moneda::': ((datos.simbolo_moneda)?datos.simbolo_moneda : ''),
          '::puntos_bloqueados::': ((!isNaN(datos.puntos_bloqueados)) ? currencyFormatter.format(datos.puntos_bloqueados, moneyFormat) : 0),
          '::nombre_nivel::': (datos.NivelNuevo && datos.NivelNuevo.nombre) ? datos.NivelNuevo.nombre : null_replacement,
          '::bonus_nivel::': (datos.NivelNuevo && datos.NivelNuevo.nombre) ? currencyFormatter.format(datos.NivelNuevo.bonus_puntos, moneyFormat) : null_replacement,
          '::icono_nivel::': (datos.NivelNuevo && datos.NivelNuevo.icono) ? datos.NivelNuevo.icono : null_replacement,
          "::font_family::": "Montserrat, Arial, sans-serif;",
          "::font_Family::": "Montserrat, Arial, sans-serif;" // Open Sans', Arial, 'sans-serif
        });

        EmailHelper.sendMail(config_cliente, to, subject, body);
      })



      return;
    }
    if (alias == medioNotifacionAlias.redimir) {


      if (Constants.tipoDePlan.sellos == config_cliente.Plan_Cliente.tipo) {
        template = '-sellos';
        EmailHelper.BuildTemplateLoyalty(config_cliente, emailConfig.template + template, idioma_usuario, (body) => {
          let tabla_premios = datos.premios_redimidos.reduce((tabla, premio, i) => {
            let div_premio = Constants.div_block_redimir_premio_sellos[idioma_usuario];
            div_premio = (!div_premio) ? "" : div_premio;
            if (premio) {
              div_premio = replaceAll(div_premio, {
                '::imagen_premio::': premio.imagen,
                '::nombre_premio::': premio.nombre,
                '::descripcion_premio::': premio.descripcion,
                '::cantidad_compra::': premio.cantidad_compra,            
              });
              tabla = tabla + "  \n " + div_premio;
            }
            return tabla
          }, "")


          body = replaceAll(body, {
            '::sucursal_nombre::': sucursal_nombre,
            '::nombre_completo::': datos.nombre_usuario,
            '::premios_disponibles::': datos.premios_disponibles,
            //'::puntos_bloqueados::': ((!isNaN(datos.puntos_bloqueados))? datos.puntos_bloqueados : 0),
            '::total_redimido::': datos.premios_redimidos_cantidad,
            '::premios_tabla::': tabla_premios,
            '::num_transaccion::': datos.num_transaccion,
            "::font_family::": "Montserrat, Arial, sans-serif;",
            "::font_Family::": "Montserrat, Arial, sans-serif;" // Open Sans', Arial, 'sans-serif
          });
          EmailHelper.sendMail(config_cliente, to, subject, body);
        })
        return;
      }



      if (Constants.tipoDePlan.premios == config_cliente.Plan_Cliente.tipo) {

        template = '-premios';

        let tabla_premios = datos.premios_redimidos.reduce((tabla, premio, i) => {
          let div_premio = Constants.div_block_redimir_premio_premios[idioma_usuario];
          div_premio = (!div_premio) ? "" : div_premio;

          div_premio = replaceAll(div_premio, {
            '::imagen_premio::': premio.imagen,
            '::precio_premio:: ': currencyFormatter.format(premio.min_puntos, moneyFormat),
            '::nombre_premio::': premio.nombre,
            '::precio_total_premio::': currencyFormatter.format((premio.min_puntos * premio.cantidad_compra), moneyFormat),
            '::descripcion_premio::': premio.descripcion,
            '::cantidad_compra::': premio.cantidad_compra,
          });

          tabla = tabla + "  \n " + div_premio;

          return tabla
        }, "")



        EmailHelper.BuildTemplateLoyalty(config_cliente, emailConfig.template + template, idioma_usuario, (body) => {
          body = replaceAll(body, {
            '::sucursal_nombre::': sucursal_nombre,
            '::premios_tabla::': tabla_premios,
            '::puntos_disponibles::': currencyFormatter.format(datos.puntos_disponibles, moneyFormat),
            '::total_redimido::': datos.premios_redimidos_cantidad,
            '::precio_total::': currencyFormatter.format(datos.precio_total, moneyFormat),
            '::simbolo_moneda::': ((datos.simbolo_moneda)?datos.simbolo_moneda : ''),
            '::nombre_completo::': datos.nombre_usuario,
            '::nombre_moneda::': datos.nombre_moneda,
            '::puntos_bloqueados::': ((!isNaN(datos.puntos_bloqueados)) ? currencyFormatter.format(datos.puntos_bloqueados, moneyFormat) : 0),
            '::num_transaccion::': datos.num_transaccion,
            "::font_family::": "Montserrat, Arial, sans-serif;",
            "::font_Family::": "Montserrat, Arial, sans-serif;" // Open Sans', Arial, 'sans-serif
          });

          EmailHelper.sendMail(config_cliente, to, subject, body);
        })
        return;
      }



      if (Constants.tipoDePlan.cashback == config_cliente.Plan_Cliente.tipo) {

        template = '-cashback';

        EmailHelper.BuildTemplateLoyalty(config_cliente, emailConfig.template + template, idioma_usuario, (body) => {
          body = replaceAll(body, {
            '::sucursal_nombre::': sucursal_nombre,
            '::puntos_disponibles::': currencyFormatter.format(datos.puntos_disponibles, moneyFormat),
            '::title::': datos.titulo,
            '::puntos_redimidos::': currencyFormatter.format(datos.puntos_redimidos, moneyFormat),
            '::simbolo_moneda::': ((datos.simbolo_moneda)?datos.simbolo_moneda : ''),
            '::nombre_moneda::': datos.nombre_moneda,
            '::nombre_completo::': datos.nombre_usuario,
            '::puntos_bloqueados::': ((!isNaN(datos.puntos_bloqueados)) ? currencyFormatter.format(datos.puntos_bloqueados, moneyFormat) : 0),
            '::num_transaccion::': datos.num_transaccion,
            "::font_family::": "Montserrat, Arial, sans-serif;",
            "::font_Family::": "Montserrat, Arial, sans-serif;" // Open Sans', Arial, 'sans-serif
          });
          EmailHelper.sendMail(config_cliente, to, subject, body);
        })
        return;
      }
    }
  } catch (error) {
    console.log(error);
    return;
  }
}





/**
 * [enviarNotifaciones description]
 * @param {[integer]}   tipo  [1,0]
 * @param {[Object]}   usuario  [{}]
 * @param {[Object]}   datos  [{}]
 * @param {[Object]}   NotificationController [{}]
 * @param {[Object]}   MedioNotifacion  [{}]
 */
module.exports.enviarCorreo = (sequelize, config_cliente, tipo, usuario, datos) => {
  if (!sequelize) {
    return Promise.reject()
  }
  let alias = (tipo == 1) ? medioNotifacionAlias.acumular : medioNotifacionAlias.redimir;
  let MedioNotifacion = MedioNotifacionModel.init(sequelize);

  return MedioNotifacion.findOne({
    where: { alias: alias }
  }).then(medioNotificacion => {
    if (medioNotificacion && medioNotificacion.correo) return envioNotifacionEmail(sequelize, config_cliente, usuario, datos, alias)
    return;
  }).catch(error => {
    console.log(error)

  })


}



/**
 * [getLabelFromString description]
 * @param {[string]}   idioma  [en,es,fr]
 * @param {[string]}   traduccion  ["something something"]
 */
module.exports.getLabelFromString = (sequelize, idioma, syslabelId, traduccion) => { return getLabelFromString(sequelize, idioma, syslabelId, traduccion); }



/**
 * [getLabel description]
 * @param {[string]}   idioma  [en,es,fr]
 * @param {[integer]}   id_label  [1,2,3,4]
 */
module.exports.getLabel = (sequelize, idioma, id_label, reuse_id_label) => {
  if (!sequelize) {
    return Promise.reject();
  }
  let SysLabel = SysLabelModel.init(sequelize);
  return sequelize.query('SELECT traduccion as traduccion FROM ' + sys_label_idioma + ' where id_idioma = ? AND id_label = ?', { replacements: [idioma, id_label], type: Sequelize.QueryTypes.SELECT })
    .then(result => { return (result[0] && result[0].traduccion) ? result[0].traduccion : "" })
    .then(traduccion => {
      return SysLabel.build().save()
        .then(sysLabel => {
          let sysLabel_use = (typeof reuse_id_label != 'undefined' && Number.isInteger(reuse_id_label)) ? reuse_id_label : sysLabel.id
          return sequelize.query('INSERT INTO ' + sys_label_idioma + ' (id_label, id_idioma, traduccion) values (?,?,?)', { replacements: [sysLabel_use, idioma, traduccion], type: Sequelize.QueryTypes.INSERT }).then(result => { return sysLabel_use })
        })
        .then(sysLabel => { return sysLabel })
    }).catch(error => {
      console.log(error);
      return;
    });
}

/**
 * [GetLabel description]
 * @param {[string]}   puntos_historicos  [en,es,fr]
 * @param {[Object]}   nivel_actual  [{}]
 * @param {[Object]}   usuario  [{}]
 */
module.exports.CheckNivel = (sequelize, config_cliente, puntos_historicos, nivel_actual, usuario) => {

  if (!sequelize || !config_cliente) {
    return Promise.reject();
  }
  let SysLabel = SysLabelModel.init(sequelize),
    TransaccionNivel = TransaccionNivelModel.init(sequelize),
    Usuario = UsuarioModel.init(sequelize),
    Nivel = NivelModel.init(sequelize);
  //Nivel.associate(Premio, InfoNivel);
  let s3_url = (config_cliente.AWSConfig.s3_url) ? config_cliente.AWSConfig.s3_url : 'no_url/';

  return Nivel.findOne({
      where: {
        $or: [{
            $and: [{
              min_puntos: { $lte: puntos_historicos },
              max_puntos: { $gte: puntos_historicos }
            }]
          },
          {
            max_puntos: { $lte: puntos_historicos }
          }
        ],
       estado: 1
      },
      order: Sequelize.literal('max_puntos DESC'),
      attributes: {
        include: [
          [Sequelize.fn('concat', s3_url, Sequelize.col('Nivel.icono')), 'icono'],
          [Sequelize.literal("(SELECT li.traduccion FROM " + sys_label_idioma + " AS li WHERE li.id_label = Nivel.id_label_nombre AND li.id_idioma='" + usuario.id_idioma + "' LIMIT 1)"), 'nombre'],
          [Sequelize.literal("(SELECT li.traduccion FROM " + sys_label_idioma + " AS li WHERE li.id_label = Nivel.id_label_descripcion AND li.id_idioma='" + usuario.id_idioma + "' LIMIT 1)"), 'descripcion'],
        ],
        exclude: [
          'id_label_descripcion',
          'estado',
          'orden',
          'icono'
        ]
      }
    }).then(nivel => {
      if (!nivel) return nivel_actual;
      return Usuario.update({
          id_nivel: nivel.id
        }, {
          where: { id: usuario.id }
        })
        .then(() => {
          if (nivel && (nivel.id == nivel_actual.id)) return nivel_actual;

          let params = {
            id_usuario: usuario.id,
            id_nivel_anterior: nivel_actual.id,
            id_nivel_actual: nivel.id
          }
          return TransaccionNivel.build(params).save()
            .then(() => { return nivel.toJSON(); });
        })
    })
    .then(nivelActual => { return nivelActual; })
    .catch(error => {
      console.log(error);
      return;
    });
}



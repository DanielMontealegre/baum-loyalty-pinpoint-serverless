//packages
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt-nodejs');

//Sequelize master y init de cliente
const sequelizeInit = require('../core/Sequelize.js');


//models master, estos modelos ya estan instanciados
const SysSessionToken = require('../models/SysSessionToken.model.js');

//Controllers


const saveTokenToDBMaster = (token, secret,sequelizeMaster) => {
    return sequelizeMaster.transaction((transaction_master) => {
         SysSessionTokenInstance = SysSessionToken.init(sequelizeMaster);
         return SysSessionTokenInstance.build({ token, secret }).save({ transaction: transaction_master })
         .then(result => (result)? result.token : null)
    }).catch(err => {
        console.log(err);
        return null;
    })
}

module.exports.createUsuarioToken = (sequelizeMaster,id, id_empresa, email, name) => {
    let secret = id + '-aplicacion-' + new Date().getTime();
    let token = jwt.sign({
        id_empresa: id_empresa,
        id: id,
        email: email,
        name: name,
        tipo: 'aplicacion'
    }, secret);
    return saveTokenToDBMaster(token, secret,sequelizeMaster);
};


module.exports.compareHash = (word, hashed ) => {
    if(typeof hashed == 'undefined' || hashed == null){
        return false;
    }
    return bcrypt.compareSync(word, hashed);
};

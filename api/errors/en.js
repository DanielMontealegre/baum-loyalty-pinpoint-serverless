/**
 * List of errors
 *
 * Example: A-0001
 */
module.exports = {
    strings: {
        required: "Required parameter",
        params_error: "Wrong parameters, check your query",
        empty: "This parameter cannot be empty",
        email: "This parameter must be a valid email",
        numbers: "This parameter must be an integer number",
        decimal: "This parameter must be a decimal number",
        boolean: "This parameter must be true or false",
        date: "This parameter must be a date and time value (YYYY-mm-dd HH:mm:ss)",
        date_format: "The required format for the date and time is (YYYY-mm-dd HH:mm:ss)",
        date_only: "This parameter must be a date (YYYY-mm-dd)",
        date_only_format: "The required format for the date is (YYYY-mm-dd)",
        greater_than_zero: "This parameter must be greater than zero",
        requires_authorization: "Requires authorization",
        authorization_failed: "Authorization failed",
        client_not_identified: "Client not identified",
        client_or_user_not_identified: "Client or User not identified",
        user_not_authorized: "Client not authorized to make this request"
    },
    system: {
        304: "Resource not modified",
        500: "Unexpected error",
        501: "Wrong Database configuration",
        400: "Invalid parameters",
        401: "Authorization is required",
        402: "Payment is required",
        403: "Forbidden resource",
        404: "Resource not found",
        409: "Inactive resource or there is a conflict with the query"
    },
    codes: {
        Usuario: {
            "0001": "There is a user already registered with the field identification",
            "0002": "This facebook id is already registered",
            "0003": "This email is already registered",
            "0004": "Wrong username or password",//"This user is not registered",
            "0005": "Wrong username or password",//"Incorrect password",
            "0006": "Wrong username or password",//"Inactive user",
            "0007": "There was an error trying to create the token, please try again",
            "0008": "User not found after being saved, try to login",
            "0009": "Invalidad invite code",
            "0010": "User not found",
            "0011": "Invalid pre-register code",
            "0012": "Pre-register code already used"
        },
        Administrador:{
           "0001": "Información incorrecta de usuario, favor volver a iniciar sesión",
        },
        Configuracion: {
            "0001": "Configuration error on this loyalty plan",
            "0002": "The reward of this configuration is invalid",
            "0003": "Bill is required for this transaction",
            "0004": "This user does not have the typed item",
            "0005": "This user does not have the amount required of this item for this transaction ",
            "0006": "You have not reached the required redemption floor",
            "0007": "Not enough points",
            "0008": "Not enough points for this transaction",
            "0009": "This item doesn't exit",
            "0010": "Not enought points for this item",
            "0011": "The quantity is greater than the stock of this item",
            "0012": "You dont have enough quantity for this item",
            '0013': "Not enough points",
            '0014': "Invalid item"
        },
        PlanLealtad: {
            "0001": "Not valid configuration found",
            "0002": "This client doesnt have this type of plan configured",

        },
        Notification: {
            "0001": "You must specify a valid platform Android o iOS",
            "0002": "This device is not registered",
            "0003": "There's been an error unsubscribing this device"
        },
        Usuario_config: {
            "0001": "This client's config is not registered or the client is not registered"
        },
        cupon: { // estos son los de regalia
            "0001": "Invalid coupon",
            "0002": "Coupon expired",
            "0003": "there's an error on this coupon's configuration",
            "0004": "Coupon exhausted",
            "0005": "Coupon already redeemed",
            "0006": "This coupon does not exist "
        },
        cuponReal: { // este es el cupon real
            "0001": "Invalid coupon",
            "0002": "Coupon expired",
            "0003": "there's an error on this coupon's configuration",
            "0004": "Coupon exhausted",
            "0005": "Coupon already redeemed",
            "0006": "This coupon does not exist",
            "0007": "This coupon is not available at this time",
            "0008": "Invalid code",
            "0009": "These codes are expired,already used or invalid",
            "0010": 'Invalid codes',
            "0011": 'Duplicated code',
            "0012": 'Duplicated codes',
            "0013" : 'You must digit a code or valid amount'
        },
        Donacion : {
             "0001":  "Not enough points ",
             "0002" : "This client doesnt have this function available",
             "0003" : "There are no fundraising campaigns right now"

        },
        canjeo: {
            "0001" : "There was a problem on the server",
            "0002" : "This request does not exist",
            "0003" : "Error updating this request, please try again later"
        },        
        sucursal:{
            "0001" : "Invalid store",
            "0002" : "Store required"
        }
    }
};
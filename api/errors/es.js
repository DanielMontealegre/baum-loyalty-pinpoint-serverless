/**
 * Lista de los errores de la aplicación
 * Se establece una numeración para los errores
 * por controlador iniciando por las siglas que representan
 * al controlador y una numeración de #### digitos numericos
 * Ejemplo: A-0001
 */
module.exports = {
    strings: {
        required: "Parámetro  requerido",
        params_error: "Parámetros incorrectos o no válidos, revise su consulta",
        empty: "Este valor no puede estar vacío",
        email: "Este valor debe ser un email válido",
        numbers: "Este valor debe ser sólo números",
        decimal: "Este valor debe ser un número válido",
        boolean: "Dato inválido",
        date: "Este valor debe ser una fecha válida (YYYY-mm-dd HH:mm:ss)",
        date_format: "Este valor debe ser una fecha válida (YYYY-mm-dd HH:mm:ss)",
        date_only: "Este valor debe ser una fecha válida (YYYY-mm-dd)",
        date_only_format: "Este valor debe ser una fecha válida (YYYY-mm-dd)",
        greater_than_zero: "Este valor debe ser mayor a cero",
        requires_authorization: "Requiere autorización",
        authorization_failed: "Fallo de autorización",
        client_not_identified: "Cliente no identificado",
        client_or_user_not_identified: "Cliente o Usuario no identificado",
        user_not_authorized: "El usuario no tiene permisos para realizar este request"
    },
    system: {
        304: "El recurso no fue modificado",
        500: "Error inesperado",
        501: "Configuración de la base de datos incorrecta",
        400: "Parámetros inválidos o con el formato incorrecto",
        401: "Autorización requerida o incorrecta",
        402: "Pago requerido",
        404: "Recurso no encontrado",
        403: "Recurso prohibido",
        409: "Recurso inactivo o existe un conflicto en la consulta"
    },
    codes: {
        Usuario: {
            "0001": "Esta identificación ya se encuentra registrada",
            "0002": "Este ID de Facebook ya se encuentra registrado",
            "0003": "Este email ya se encuentra registrado",
            "0004": "Credenciales incorrectas",//"Usuario no registrado",
            "0005": "Credenciales incorrecta",//"Contraseña incorrecta",
            "0006": "Credenciales incorrecta",//"Usuario inactivo",
            "0007": "Ocurrió un error en la creación del token, por favor inténtelo de nuevo",
            "0008": "No se encontró el Usuario después de guardar, intente loguear",
            "0009": "Código de invitación inválido",
            "0010": "Usuario no registrado",
            "0011": "Codigo de pre-registro inválido",
            "0012": "Codigo de pre-registro ya utilizado"
        },
        Administrador:{
           "0001": "Información incorrecta de usuario, favor volver a iniciar sesión",
        },
        Configuracion: {
            "0001": "Error en la configuración del plan",
            "0002": "El premio de esta configuración no existe",
            "0003": "Número de factura requerido",
            "0004": "No dispone del premio digitado",
            "0005": "No dispone de la cantidad necesaria de este premio para realizar el canjeo",
            "0006": "No ha alcanzado el piso de redención ",
            "0007": "Saldo insuficiente", // Este usuario no tiene saldo insuficiente
            "0008": "Saldo insuficiente", //El usurio no tiene suficiente saldo para realizar esta transacción
			"0009": "El premio ingreaso no existe",
			"0010": "No tiene los suficientes puntos para este premio",
            "0011": "Cantidad de premios no disponible",// La cantidad es mayor al stock
            "0012": "Sin premios suficientes para el canjeo", //"No tiene premios suficientes acumulados para esta transacción"
            '0013': "Saldo insuficiente",
            '0014': "Premio inválido"
        },
       PlanLealtad : {
            "0001": "Este cliente no tiene un plan válido configurado",
            "0002": "Este cliente no posee este tipo de plan en su configuración", 

        },
        Notification: {
            "0001": "Debe especificar una plataforma válida Android o iOS",
            "0002": "Este dispositivo no esta registrado",
            "0003": "Hubo un error a la hora de desvincular este dispositivo"
        },
        Usuario_config: {
            "0001": "El cliente y/o su configuración no existen"
        },
        cupon : {
            "0001":  "Código de regalía inválido",
            "0002" : "Código de regalía expirado",
            "0003" : "Hay un error en la configuración de la regalía",
            "0004" : "Regalía agotada",
            "0005" : "Regalía ya redimida",
            "0006" : "No existe esta regalía"  
        },
        cuponReal : {
            "0001":  "Cupón inválido",
            "0002" : "Cupón expirado",
            "0003" : "Hay un error en la configuración del cupón",
            "0004" : "Cupón agotado",
            "0005" : "Límite de usos alcanzado",
            "0006" : "No existe este cupón",
            "0007" : "Este cupón no esta disponible en este horario",
            "0008" : "Código inválido",
            "0009" : "Códigos expirados, utilizados o inválidos",
            "0010" : 'Códigos inválidos',
            "0011" : 'Código de cupón repetido',
            "0012" : 'Códigos repetidos',
            "0013" : 'Debe ingresar mínimo un cupón o un monto valido',
        },
          Donacion : {
             "0001": "Saldo insuficiente",
             "0002" : "Este cliente no tiene esta función habilitada",
             "0003" : "No hay campañas de donación en este momento"

        },
        canjeo: {
            "0001" : "Hubo un problema en el servidor",
            "0002" : "No existe esta solicitud",
            "0003" : "Fallo actualizando la solicitud de canjeo. Por favor interlo más tarde."
        },
        sucursal:{
            "0001" : "Sucursal invalida",
            "0002" : "Sucursal requerida"
        }
    }
};
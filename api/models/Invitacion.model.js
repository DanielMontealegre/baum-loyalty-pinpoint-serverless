const Sequelize = require('sequelize');

//id_usuario_invita, id_usuario_invitado, cant_regalias_invita, cant_regalias_invitado, fecha_vencimiento_invita, fecha_vencimiento_invitado, fecha_registro


const init = (sequelize) => {
    const Invitacion = sequelize.define('Invitacion', {
        id_usuario_invita: {
            type: Sequelize.INTEGER,
            primaryKey: true
        },
        id_usuario_invitado: {
            type: Sequelize.INTEGER,
            primaryKey: true
        },
        cant_regalias_invita: {
            type: Sequelize.INTEGER
        },
        cant_regalias_invitado: {
            type: Sequelize.INTEGER
        },
        fecha_vencimiento_invita: {
            type: Sequelize.DATE
        },
        fecha_vencimiento_invitado: {
            type: Sequelize.DATE
        },
        fecha_registro: {
            type: Sequelize.DATE
        }
    }, {
        // don't add the timestamp attributes (updatedAt, createdAt)
        timestamps: false,

        // don't delete database entries but set the newly added attribute deletedAt
        // to the current DATE (when deletion was done). paranoid will only work if
        // timestamps are enabled
        //paranoid: true,

        // don't use camelcase for automatically added attributes but underscore style
        // so updatedAt will be updated_at
        underscored: true,
        // disable the modification of table names; By default, sequelize will automatically
        // transform all passed model names (first parameter of define) into plural.
        // if you don't want that, set the following
        freezeTableName: true,
        // define the table's name
        tableName: 'pl_invitacion'
    });

    Invitacion.query = (query, spread) => {
        if (spread) {
            return sequelize.query(query);
        } else {
            return sequelize.query(query, { type: sequelize.QueryTypes.SELECT });
        }
    };
    return Invitacion;
};

module.exports.init = init;
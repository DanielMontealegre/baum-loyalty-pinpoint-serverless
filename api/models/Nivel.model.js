const Sequelize = require('sequelize');

/*
id, int
id_label_nombre, int FK sys label
id_label_descripcion, int FK sys label
icono, String
min_puntos, FLOAT
max_puntos, FLOAT 
porc_acumulacion_saldo, FLOAT 
id_premio, int FK pl premio
estado, int
orden, int

*/

const init = (sequelize) => {
    const Nivel = sequelize.define('Nivel', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        id_label_nombre: {
            type: Sequelize.INTEGER
        },
        id_label_descripcion: {
            type: Sequelize.INTEGER
        },
        icono: {
            type: Sequelize.STRING
        },
        min_puntos: {
            type: Sequelize.FLOAT
        },
        max_puntos: {
            type: Sequelize.FLOAT
        },
        porc_acumulacion_puntos: {
            type: Sequelize.FLOAT
        },
        id_premio: {
            type: Sequelize.INTEGER
        },
        estado: {
            type: Sequelize.INTEGER
        },
        orden: {
            type: Sequelize.INTEGER
        },
        bonus_puntos: {
            type: Sequelize.FLOAT
        }

    }, {

        // don't add the timestamp attributes (updatedAt, createdAt)
        timestamps: false,

        // don't delete database entries but set the newly added attribute deletedAt
        // to the current DATE (when deletion was done). paranoid will only work if
        // timestamps are enabled
        //paranoid: true,

        // don't use camelcase for automatically added attributes but underscore style
        // so updatedAt will be updated_at
        underscored: true,
        // disable the modification of table names; By default, sequelize will automatically
        // transform all passed model names (first parameter of define) into plural.
        // if you don't want that, set the following
        freezeTableName: true,
        // define the table's name
        tableName: 'pl_nivel'
    });

    Nivel.associate = (Premio, ReglaBeneficio, ValorSellos) => {
        if (Premio)
            Nivel.belongsTo(Premio, { as: 'premio', foreignKey: 'id_premio', targetKey: 'id' });
        if (ReglaBeneficio) {
            Nivel.hasMany(ReglaBeneficio, { as: 'beneficios', foreignKey: 'id_nivel', targetKey: 'id' });
            Nivel.hasMany(ReglaBeneficio, { as: 'reglas', foreignKey: 'id_nivel', targetKey: 'id' });
        }
        if (ValorSellos)
            Nivel.hasMany(ValorSellos, { as: 'ValorSellos', foreignKey: 'id_nivel', targetKey: 'id' });
        //Cliente.belongsTo(Canton, {as: 'canton', foreignKey: 'id_canton', targetKey: 'id'});
    };
    Nivel.query = (query, spread) => {
        if (spread) {
            return sequelize.query(query);
        } else {
            return sequelize.query(query, { type: sequelize.QueryTypes.SELECT });
        }
    };
    return Nivel;
};

module.exports.init = init;
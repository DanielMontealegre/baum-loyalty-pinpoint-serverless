const Sequelize = require('sequelize');
/*
id, integer 
id_usuario, integer
id_label_nombre_premio, integer
cantidad_premio, integer
cantidad_puntos, integer
tipo, integer
id_label_descripcion, integer
id_sucursal, integer
created_at, DATE

*/

const init = (sequelize) => {
    const TransaccionPremio = sequelize.define('TransaccionPremio', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        num_factura: {
            allowNull: true,
            defaultValue: null,
            type: Sequelize.STRING
        },
        id_usuario: {
            type: Sequelize.INTEGER
        },
        id_factura: {
            type: Sequelize.INTEGER
        },
        id_admin: {
            type: Sequelize.INTEGER
        },
        premios: {
            type: Sequelize.INTEGER
        },
        puntos: {
            type: Sequelize.FLOAT
        },
        puntos_disponibles: {
            type: Sequelize.FLOAT
        },
        premios_disponibles: {
            type: Sequelize.INTEGER
        },
        tipo: {
            type: Sequelize.INTEGER
        },
        id_label_descripcion: {
            type: Sequelize.INTEGER
        },
        id_label_titulo:{
            type: Sequelize.INTEGER
        },
        id_sucursal: {
            type: Sequelize.INTEGER
        },
        created_at: {
            type: Sequelize.DATE
        }
    }, {

        // don't add the timestamp attributes (updatedAt, createdAt)
        timestamps: false,

        // don't delete database entries but set the newly added attribute deletedAt
        // to the current DATE (when deletion was done). paranoid will only work if
        // timestamps are enabled
        //paranoid: true,

        // don't use camelcase for automatically added attributes but underscore style
        // so updatedAt will be updated_at
        underscored: true,
        // disable the modification of table names; By default, sequelize will automatically
        // transform all passed model names (first parameter of define) into plural.
        // if you don't want that, set the following
        freezeTableName: true,
        // define the table's name
        tableName: 'pl_transaccion_premio'
    });

    TransaccionPremio.associate = (Usuario, Sucursal, PremioTransaccionPremio,Factura) => {
        if (Usuario)
            TransaccionPremio.belongsTo(Usuario, { as: 'usuario', foreignKey: 'id_usuario', targetKey: 'id' });
        if (Sucursal)
            TransaccionPremio.belongsTo(Sucursal, { as: 'sucursal', foreignKey: 'id_sucursal', targetKey: 'id' });
        if (PremioTransaccionPremio)
            TransaccionPremio.hasMany(PremioTransaccionPremio, { as: 'premios_transaccion', foreignKey: 'id_transaccion', targetKey: 'id', onDelete: 'cascade', hooks: true }); //
        if(Factura){
            TransaccionPremio.belongsTo(Factura, { as: 'factura', foreignKey: 'id_factura', targetKey: 'id', onDelete: 'cascade', hooks: true }); //
        }
    };

    TransaccionPremio.query = (query, spread) => {
        if (spread) {
            return sequelize.query(query);
        } else {
            return sequelize.query(query, { type: sequelize.QueryTypes.SELECT });
        }
    };

    return TransaccionPremio;
}
module.exports.init = init;
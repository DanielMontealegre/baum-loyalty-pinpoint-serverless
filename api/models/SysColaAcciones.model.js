const Sequelize = require('sequelize');

/*
id, 
alias, 
data,
estado, 
created_at, 
updated_at, 
completed_at
*/

const init = (sequelize) => {
    const SysColaAcciones = sequelize.define('SysColaAcciones', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        alias: {
            type: Sequelize.STRING
        },
        data: {
            type: Sequelize.STRING
        },
        estado: {
            type: Sequelize.STRING
        },
        error:{
            type: Sequelize.STRING
        },
        intentos: {
            type: Sequelize.INTEGER
        },
        completed_at: {
            type: Sequelize.DATE
        },
        updated_at: {
            type: Sequelize.DATE
        },
        created_at: {
            type: Sequelize.DATE
        }
    }, {
        // don't add the timestamp attributes (updatedAt, createdAt)
        timestamps: true,

        // don't delete database entries but set the newly added attribute deletedAt
        // to the current DATE (when deletion was done). paranoid will only work if
        // timestamps are enabled
        //paranoid: true,

        // don't use camelcase for automatically added attributes but underscore style
        // so updatedAt will be updated_at
        underscored: true,
        // disable the modification of table names; By default, sequelize will automatically
        // transform all passed model names (first parameter of define) into plural.
        // if you don't want that, set the following
        freezeTableName: true,
        // define the table's name
        tableName: 'sys_cola_acciones'
    });
    SysColaAcciones.query = (query, spread) => {
        if (spread) {
            return sequelize.query(query);
        } else {
            return sequelize.query(query, { type: sequelize.QueryTypes.SELECT });
        }
    };
    return SysColaAcciones;
};

module.exports.init = init;
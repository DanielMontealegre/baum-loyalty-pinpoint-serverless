const Sequelize = require('sequelize');
//id, alias, label, correo, mensaje_push
const init = (sequelize) => {
    const MedioNotifacion = sequelize.define('MedioNotifacion', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        alias: {
            type: Sequelize.STRING
        },
        label: {
            type: Sequelize.STRING
        },
        correo: {
            type: Sequelize.BOOLEAN
        },
        mensaje_push: {
            type: Sequelize.BOOLEAN
        }
    }, {
        // don't add the timestamp attributes (updatedAt, createdAt)
        timestamps: false,
        // don't delete database entries but set the newly added attribute deletedAt
        // to the current DATE (when deletion was done). paranoid will only work if
        // timestamps are enabled
        //paranoid: true,

        // don't use camelcase for automatically added attributes but underscore style
        // so updatedAt will be updated_at
        underscored: true,
        // disable the modification of table names; By default, sequelize will automatically
        // transform all passed model names (first parameter of define) into plural.
        // if you don't want that, set the following
        freezeTableName: true,
        // define the table's name
        tableName: 'not_medio_notificacion'
    });

    MedioNotifacion.query = (query, spread) => {
        if (spread) {
            return sequelize.query(query);
        } else {
            return sequelize.query(query, { type: sequelize.QueryTypes.SELECT });
        }
    };
    return MedioNotifacion;

};

module.exports.init = init;
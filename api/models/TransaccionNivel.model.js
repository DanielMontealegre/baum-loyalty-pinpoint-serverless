const Sequelize = require('sequelize');

const init = (sequelize) => {
    const TransaccionNivel = sequelize.define('TransaccionNivel', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        id_usuario: {
            type: Sequelize.INTEGER
        },
        id_nivel_anterior: {
            type: Sequelize.INTEGER
        },
        id_nivel_actual: {
            type: Sequelize.INTEGER
        }
    }, {
        // don't add the timestamp attributes (updatedAt, createdAt)
        // timestamps: false,
        // don't delete database entries but set the newly added attribute deletedAt
        // to the current DATE (when deletion was done). paranoid will only work if
        // timestamps are enabled
        //paranoid: true,

        // don't use camelcase for automatically added attributes but underscore style
        // so updatedAt will be updated_at
        underscored: true,
        // disable the modification of table names; By default, sequelize will automatically
        // transform all passed model names (first parameter of define) into plural.
        // if you don't want that, set the following
        freezeTableName: true,
        // define the table's name
        tableName: 'pl_transaccion_nivel'
    });

    TransaccionNivel.associate = (NivelAnterior, NivelActual, Usuario) => {
        TransaccionNivel.belongsTo(NivelAnterior, { as: 'transaccionNivelAnterior', foreignKey: 'id_nivel_anterior', targetKey: 'id' });
        TransaccionNivel.belongsTo(NivelActual, { as: 'transaccionNivelActual', foreignKey: 'id_nivel_actual', targetKey: 'id' });
        TransaccionNivel.belongsTo(Usuario, { as: 'usuario', foreignKey: 'id_usuario', targetKey: 'id' });
    };
    
    TransaccionNivel.query = (query, spread) => {
        if (spread) {
            return sequelize.query(query);
        } else {
            return sequelize.query(query, { type: sequelize.QueryTypes.SELECT });
        }
    };

    return TransaccionNivel;

};

module.exports.init = init;

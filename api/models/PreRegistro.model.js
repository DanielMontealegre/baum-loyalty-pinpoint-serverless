const Sequelize = require('sequelize');

/*
    id, int,
    id_usuario,
    id_externo, 
    codigo, 
    nombre,  
    apellido, 
    email,  
    telefono, ,
    estado, 
    created_at, 
    updated_at, 
    */

const init = (sequelize) => {
    const PreRegistro = sequelize.define('PreRegistro', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        id_usuario: {
            type: Sequelize.INTEGER
        },
        id_externo: {
            type: Sequelize.STRING
        },
        email: {
            type: Sequelize.STRING
        },
        telefono: {
            type: Sequelize.STRING
        },
        codigo: {
            type: Sequelize.STRING
        },
        apellido: {
            type: Sequelize.STRING
        },
        nombre: {
            type: Sequelize.STRING
        },
        estado: {
            type: Sequelize.INTEGER
        },
        created_at: {
            type: Sequelize.DATE
        },
        updated_at: {
            type: Sequelize.DATE
        }
    }, {
        // don't add the timestamp attributes (updatedAt, createdAt)
        timestamps: true,

        // don't delete database entries but set the newly added attribute deletedAt
        // to the current DATE (when deletion was done). paranoid will only work if
        // timestamps are enabled
        //paranoid: true,

        // don't use camelcase for automatically added attributes but underscore style
        // so updatedAt will be updated_at
        underscored: true,
        // disable the modification of table names; By default, sequelize will automatically
        // transform all passed model names (first parameter of define) into plural.
        // if you don't want that, set the following
        freezeTableName: true,
        // define the table's name
        tableName: 'pl_usuario_pre_registro'
    });

    PreRegistro.associate = () => {
    };

    PreRegistro.query = (query, spread) => {
        if (spread) {
            return sequelize.query(query);
        } else {
            return sequelize.query(query, { type: sequelize.QueryTypes.SELECT });
        }
    };

    return PreRegistro;
};

module.exports.init = init;
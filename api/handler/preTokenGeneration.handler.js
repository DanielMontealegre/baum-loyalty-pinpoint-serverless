//Helpers
const ConfigHelper = require('../helpers/Config.helper.js');
const SecurityHelper = require('../helpers/Security.helper.js');
const sequelizeInit = require('../core/Sequelize.js');

//Controller
const UsuarioController = require('../controllers/Usuario.controller.js');


const getDataFromEvent = (event) => {
    const userAttributes = event.request.userAttributes;
    const param = {
        sub: userAttributes['sub'],
        email: userAttributes['email'],
        nombre: userAttributes['name'],
        id_idioma: ((userAttributes['custom:lang']) ? userAttributes['custom:lang'] : 'es')
    };
    return param;
}

const data_lealto = (token ='', isValidLogin = false, error = 'No error') => {
    return {
        ['claimsToAddOrOverride']: {
            ["lealtoToken"] :token,
            ["errorMessage"] :error,
            ["isValidLogin"] :isValidLogin
        }
    } 
}

const errorMessage = (errorMsj) => {
    const mensaje =  {
        valido: false, 
        error: { 
            mensaje: errorMsj 
        } 
    };
    return JSON.stringify(mensaje);
}

const preTokenGenerationHandler = async(event,dbConfig) => {
    console.log(`=======event==> ${JSON.stringify(event)} ==================>`);
    event.response.claimsOverrideDetails = ( event.response.claimsOverrideDetails)?  event.response.claimsOverrideDetails : {};
    try {
        const result = await ConfigHelper.getConfigCliente(event,dbConfig);

        if (!result.valido) {
            event.response.claimsOverrideDetails =  data_lealto('',false,result.error.mensaje);
            return event; 

        }
        const sequelizeMasterCon = sequelizeInit.init(dbConfig);
        const { sequelizeCliente, config_cliente } = result;
        const { id_cliente } = config_cliente;
        const { sub, email, name, id_idioma } = getDataFromEvent(event);


        const controllerUsuario = new UsuarioController(sequelizeCliente,sequelizeMasterCon,config_cliente, id_idioma);


        await controllerUsuario.AppAsignarSubAUsuario(sub,email); // este metodo vuelve a setear el sub de cognito y crea o hace update del endpoint de correo

        const usuario = await controllerUsuario.AppGetUsuarioBySub(sub);

        if (!usuario) {
            event.response.claimsOverrideDetails =  data_lealto('',false,'Usuario invalido - 0001'); 
            return event;
        }

        const token = await SecurityHelper.createUsuarioToken(sequelizeMasterCon,usuario.id, id_cliente, usuario.email, usuario.nombre);

         event.response.claimsOverrideDetails =  data_lealto(token,true,''); 
        return event;
    } catch (error) {
        console.log(error);
        event.response.claimsOverrideDetails =  data_lealto('',false,'Error inesperado - 0002'); 
        //throw new Error(errorMessage('Error inesperado - 0002'));
        return event;
    }

}

module.exports.handler = preTokenGenerationHandler;


//para text local
/*
const eventExample = { version: '1',
        triggerSource: 'TokenGeneration_Authentication',
        region: 'us-east-1',
        userPoolId: 'us-east-1_tB9jKybPP',
        userName: 'harley@baum.digital',
        callerContext: 
        { awsSdkVersion: 'aws-sdk-unknown-unknown',
        clientId: 'crrq6dsaplhahtea78d6haml6' },
        request: 
        { userAttributes: 
        { sub: 'df051b84-fc02-4100-a716-72f4cedeb321',
        email_verified: 'true',
        'cognito:user_status': 'CONFIRMED',
        'custom:isSocial': '0',
        name: 'Harley',
        'custom:receiveNotifications': '1',
        family_name: 'Espinoza',
        email: 'harley@baum.digital' },
        groupConfiguration: 
        { groupsToOverride: [],
        iamRolesToOverride: [],
        preferredRole: null } },
        response: { claimsOverrideDetails: null }
 }



postAuthenticationHandler(eventExample).then(result => {
    console.log(JSON.stringify(result));
    return result;
}).catch(err => {
    console(err);
})

*/
//Helpers
const ConfigHelper = require('../helpers/Config.helper.js');
const sequelizeInit = require('../core/Sequelize.js');

//Controller
const UsuarioController = require('../controllers/Usuario.controller.js');

const preSignUpHandler = async(event,dbConfig) => {
    console.log(`=======event==> ${JSON.stringify(event)} ==================>`);
	if(event.triggerSource == 'PreSignUp_SignUp'){
		const { userPoolId, request: { userAttributes }, userName } = event;
		const { email, birthdate } = userAttributes;
		const identificacion  = userAttributes['custom:identificacion']; 
        const id_idioma = ((userAttributes['custom:id_idioma']) ? userAttributes['custom:id_idioma'] : 'es');

        const result = await ConfigHelper.getConfigCliente(event,dbConfig);
        if (!result.valido) {
            throw new Error(JSON.stringify(result));
        }
        const sequelizeMasterCon = sequelizeInit.init(dbConfig);
        const { sequelizeCliente, config_cliente } = result;
        const controllerUsuario = new UsuarioController(sequelizeCliente,sequelizeMasterCon,config_cliente, id_idioma);
        const response = await controllerUsuario.validarSignUp({ userName, identificacion, email, birthdate });
        if(response.valido){
        	return event;
        }else{
        	throw new Error(JSON.stringify(response));
        }
	}else{
		return event;
	}
}

module.exports.handler = preSignUpHandler;

const response = require('cfn-response');
const AWS = require('aws-sdk');
const SSM = new AWS.SSM();
const pinpoint = new AWS.Pinpoint({ apiVersion: '2016-12-01', region: 'us-east-1' });

const GCM = 'GCM';
const EMAIL = 'EMAIL';
const APNS_CERTIFICATE = "APNS_CERTIFICATE";
const APNS_PRIVATEKEY = "APNS_PRIVATEKEY";
const APNS_SANDBOX = "APNS_SANDBOX";

const timeOutEmit = 180000 // 3 minutos
const timeOutEmitError = 60000 // 1 minuto
exports.handler = function(event, context) {
  if (event.RequestType == 'Delete') {
    console.log('--- DELETE CALL ---');
    response.send(event, context, response.SUCCESS);
    return;
  }
  if (event.RequestType == 'Update') {
    console.log('--- UPDATE CALL ---');
    response.send(event, context, response.SUCCESS);
    return;
  }
  if (event.RequestType == 'Create') {
    console.log('--- CREATE CALL ---');
    const { appName, FromAddress } = event.ResourceProperties
    let responseData = {};

    let ApplicationIdAux;
    let ApplicationId;
    const params = {
      CreateApplicationRequest: {
        Name: appName
      }
    };
    return pinpoint.createApp(params).promise()
      .then((res) => {
        responseData = res.ApplicationResponse;
        ApplicationId = responseData.Id;
        return ApplicationId;
      })
      .then(() => updateApnsChannel({ ApplicationId }))
      .then(() => updateEmailChannel({ ApplicationId }))
      .then(() => updateGcmChannel({ GCMChannelRequest: { Enabled: true }, ApplicationId }))
      .then(() => {
              response.send(event, context, response.SUCCESS, responseData);
      })
      then(() => { // esto no deberia pasar nunca, es solo para dar 3 minutos de tiempo de que el response se mande a cloudformation
        return new Promise(function(resolve, reject) {
          setTimeout(function() {
            console.log(`event => defaultResult ${timeOutEmit} - 3 minutos`);
            resolve('No se envio la respuesta de cloudFormation');
          }, timeOutEmit);
        });
      })
      .catch((err) => {
        console.log(err.stack);
        responseData = { Error: err };
        response.send(event, context, response.FAILED, responseData);
        setTimeout(() => {
                  throw err;
        },timeOutEmitError)
      });
  }
};


const updateApnsChannel = async (params = {}) => {
  try{
    const  CertificateAux = await getChannelConfig(APNS_CERTIFICATE);
    const  PrivateKeyAux = await getChannelConfig(APNS_PRIVATEKEY);
    const  isSandbox = await getChannelConfig(APNS_SANDBOX);
    const { ApplicationId } = params

    const method = (isSandbox == true || isSandbox == 'true')? 'updateApnsSandboxChannel': 'updateApnsChannel'
    const request = (isSandbox == true || isSandbox == 'true')? 'APNSSandboxChannelRequest' : 'APNSChannelRequest'


    const Certificate = unescape(replaceAll(CertificateAux, "'", ""));
    const PrivateKey = unescape(replaceAll(PrivateKeyAux, "'", ""));

    const result = await pinpoint.updateApnsChannel({
      APNSChannelRequest: { /* required */
        Certificate: Certificate,
        DefaultAuthenticationMethod: 'Certificate',
        Enabled: true,
        PrivateKey: PrivateKey,
      },
      ApplicationId
    }).promise();
    return result;
  }catch(error){
      console.log('Error => updateApnsChannel')
      console.log(error); 
      return {};
  }
};

const updateGcmChannel = async (params = {}) => {
  try{
    const GCMApikey = await getChannelConfig(GCM);
    if(process.env.NODE_ENV == 'dev') console.log(`stringify GCMApikey => ${GCMApikey}`);
    params.GCMChannelRequest['ApiKey'] =  GCMApikey;
    const result = await pinpoint.updateGcmChannel(params).promise();
    return result;
  }catch(error){
      console.log('Error => updateGcmChannel')
      console.log(error); 
      return {};
  }
};

const updateEmailChannel = async (params = {}) => {
  try{
    const stringify = await getChannelConfig(EMAIL)
    if(process.env.NODE_ENV == 'dev') console.log(`stringify updateEmailChannel => ${stringify}`);
    const paramsFinal = JSON.parse(stringify);
    const result = await pinpoint.updateEmailChannel({ EmailChannelRequest:{ Enabled: true, ...paramsFinal },...params }).promise();
    return result;
  }catch(error){
      console.log('Error => updateEmailChannel')
      console.log(error); 
      return {};
  }
};

const getChannelConfig = async (channel,Path) => {
    let config_route = '';
    switch(channel){
      case APNS_CERTIFICATE:
            config_route = process.env.PATH_APNS_CERTIFICATE;
            break;
      case APNS_PRIVATEKEY:
            config_route = process.env.PATH_APNS_PRIVATEKEY;
            break;
      case APNS_SANDBOX:
            config_route = process.env.APNS_IS_SANDBOX;
            break;
      case GCM: 
            config_route = process.env.PATH_GCM;
            break;
      case EMAIL:
            config_route = process.env.PATH_EMAIL;
            break;
    }
    const envVariable = await getParametersByPathAll(null,[]);
    return getValueFromArray(envVariable, config_route);
};

const getParametersByPathAll = async(NextToken, result = []) => {
    const params = {
        Path: process.env.SSM_PATH,
        WithDecryption: true,
        NextToken      
    };

    const envVariable = await SSM.getParametersByPath(params)
        .promise()
        .catch(error => {
            console.log(`Error SSM.getParametersByPath  => ${error}`);
            return { Parameters: []};
        });
    if(!envVariable.NextToken){
        return result.concat(envVariable.Parameters);
    }
    else{
        return await getParametersByPathAll(envVariable.NextToken, result.concat(envVariable.Parameters));
    }
};

const getValueFromArray = (array, name) => {
    const index = array.findIndex(e => e.Name == name);
    return (index < 0) ? '' : array[index].Value;
};


function replaceAll(str, find, replace) {
    return str.replace(new RegExp(find, 'g'), replace);
}
//Helpers
const ConfigHelper = require('../helpers/Config.helper.js');
const sequelizeInit = require('../core/Sequelize.js');

//Controller
const NotificacionController = require('../controllers/Notificacion.controller.js');


const customMessageHandler = async(event,dbConfig) => {
  try {
        if(event.triggerSource === "CustomMessage_SignUp" || event.triggerSource === "CustomMessage_ForgotPassword" || event.triggerSource === 'CustomMessage_ResendCode'){
            console.log(`=======event==> ${JSON.stringify(event)} ==================>`);
            const result = await ConfigHelper.getConfigCliente(event,dbConfig);

            if (!result.valido) {
              console.log(result.error);
              return event;
            }
            const { config_cliente } = result;
            const codigo = event.request.codeParameter;
            const user = event.request.userAttributes;
            const lang = user['custom:id_idioma'];

            const type = (event.triggerSource === "CustomMessage_SignUp" ) ? 'confirmacion' : 'verificacion'; 
            // verificacion es CustomMessage_ForgotPassword 
            //FORCE_CHANGE_PASSWORD
            //CustomMessage_AdminCreateUser
            //CustomMessage_UpdateUserAttribute

            const message = await NotificacionController.createVerificationCustomMessage(config_cliente, lang, type, user, codigo);

            event.response.smsMessage = message.smsMessage;
            event.response.emailSubject = message.emailSubject;
            event.response.emailMessage = message.emailMessage;  
        }
        return event;
  } catch (error) {
        console.log(error);
        return event;
  }

}





module.exports.handler = customMessageHandler;

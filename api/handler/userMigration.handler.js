
const ConfigHelper = require('../helpers/Config.helper.js');
const sequelizeInit = require('../core/Sequelize.js');
//Controller
const UsuarioController = require('../controllers/Usuario.controller.js');

module.exports.handler = async (event, dbConfig) => {
    const result = await ConfigHelper.getConfigCliente(event,dbConfig);

    const errorResponse = (error = { mensaje: 'Unexpected error'}, status = 500, code = 'S500') => {
        valido: false,
        status,
        error
    };

    if (!result.valido) {
            throw new Error(JSON.stringify(result));
    }
    else if ( event.triggerSource == "UserMigration_Authentication" || event.triggerSource == "UserMigration_ForgotPassword") {
        // authenticate the user with your existing user directory service
        const isForgotPassword = (event.triggerSource == "UserMigration_ForgotPassword")? true : false;
        const requestPassword = (event.triggerSource == "UserMigration_ForgotPassword")? '' : event.request.password;

        const sequelizeMasterCon = sequelizeInit.init(dbConfig);
        const { sequelizeCliente, config_cliente } = result;
        const controllerUsuario = new UsuarioController(sequelizeCliente,sequelizeMasterCon,config_cliente, 'es');

        const resultMigrationObj = await controllerUsuario.AppUserMigration(event.userName, requestPassword, isForgotPassword);
        const { user, responseCognito, valid } = resultMigrationObj;
        if ( user && valid ) {
            responseCognito.userAttributes['email_verified'] = "true";
            event.response = responseCognito;
            console.log({ event })
            return event;
        }
        else {
            // Return error to Amazon Cognito
            const { error, status, code  } = resultMigrationObj;
            console.log({ error, status, code})
            throw new Error(JSON.stringify(errorResponse(error,status,code)));
        }
    }
    else {       
        // Return error to Amazon Cognito
         throw new Error(JSON.stringify(errorResponse({mensaje: ("Bad triggerSource " + event.triggerSource) })));
    }
};


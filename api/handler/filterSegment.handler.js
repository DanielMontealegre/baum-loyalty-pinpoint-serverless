const R = require('ramda');
//Helpers
const ConfigHelper = require('../helpers/Config.helper.js');
const sequelizeInit = require('../core/Sequelize.js');

//Constants
const Constants = require('../../config.js').Constants;
const TIPO_PLAN = Constants.tipoDePlan;
const TIPO_TRANSACION = Constants.tipoTransaccion;
const AVAILABLE_FOR_CAMPAING = 'available_for_campaing'; // con 1 y 0
const SUB_COGNITO = 'sub_cognito'
//utility
const isValidValue = R.complement(R.either(R.isNil, R.isEmpty));
const withOutSpacesArray = (x) => !isValidValue(x);


const filterSegmentHandler = async(event = {}, dbConfig = null, callback = () => { }) => {
    const { ApplicationId: applicationId, CampaignId: campaignIdPinpoint, Endpoints: endpoints } = event;
    if(process.env.NODE_ENV == 'test'){
        console.log({ event: JSON.stringify(event) });
    }
    if(isValidValue(applicationId) && isValidValue(campaignIdPinpoint)){
        const result = await ConfigHelper.getConfigCliente(event, dbConfig, true);
        if (!result.valido) {
            throw new Error(JSON.stringify(result));
        }
        const { sequelizeCliente, config_cliente: configCliente } = result;

        const campaingDetails = await getCampaingDetails({ sequelizeCliente, configCliente, campaignIdPinpoint });

        if(!campaingDetails){
           throw new Error('[Invalid campaing] => no filters');
        }

        const filterType = R.prop('tipo',campaingDetails);
        let resultUsers = {};
        if(!isValidValue(filterType)){
            throw new Error(`[Invalid filter type] ${filterType}`);
        }
        if( filterType == 1){
            resultUsers = await getCampaingUsersByFilters({ sequelizeCliente, configCliente,  campaingDetails });
        }
        if( filterType == 2){
            resultUsers = await getCampaingUsersBySegment({ sequelizeCliente, campaignIdPinpoint });
        }
        if(!R.prop('valido',resultUsers)){
            throw new Error(R.prop('error',resultUsers));
        }
        const subsCognito = R.prop('data',resultUsers);

        const finalEndpoints = filterEndpoints(endpoints,subsCognito);
        console.log({ finalEndpoints: JSON.stringify(finalEndpoints) })
        //callback(null, finalEndpoints);
        return finalEndpoints;
	}else{
		throw new Error('[Invalid event] => no ApplicationId || CampaignId');
	}
};

const getCampaingUsersByFilters = async ({ sequelizeCliente, configCliente = null,  campaingDetails = {} }) => {
        console.log(`[getCampaingUsersByFilters]`)
        if(!isValidValue(configCliente)){
            return { valido: false, error: 'Invalid config'};
        }
        if(!isValidValue(sequelizeCliente)){
            return { valido: false, error: 'Invalid sequelizeCliente'};
        }
        let filtros = { ...campaingDetails };
        let filtro = "";
        let filtroTransacciones = "";
        let queryAdicional = "";
        //filtro de ventas
        let filtroVentas = R.pathOr(0, ['ventas'], filtros);
        let filtroFecha = R.dropWhile(withOutSpacesArray , R.pathOr('', ['rango_fechas'], filtros).split('-'));
        let filtroSucursal = R.dropWhile(withOutSpacesArray , R.pathOr('', ['sucursales'], filtros).split(','));
        let filtroProducto = R.dropWhile(withOutSpacesArray , R.pathOr('', ['sku_productos'], filtros).split(','));
        let filtroCupon = R.dropWhile(withOutSpacesArray , R.pathOr('', ['cupones'], filtros).split(','));
        let idioma = R.dropWhile(withOutSpacesArray , R.pathOr('',['idioma'],filtros).split(','));
        let genero = R.dropWhile(withOutSpacesArray ,  R.pathOr('',['generos'],filtros).split(','));
        let edad = R.dropWhile(withOutSpacesArray , R.pathOr('',['edades'],filtros).split(','));
        let nivel = R.dropWhile(withOutSpacesArray , R.pathOr('',['niveles'],filtros).split(','));


        console.log({ filtros, filtroFecha })

        if(filtroVentas == 1){
            //consumo
            let plan = R.prop('Plan_Cliente',configCliente);
            let tablaTransacciones = plan == TIPO_PLAN.cashback ? 'pl_transaccion_saldo' : 'pl_transaccion_premio';
            let tipoAcumulacion = TIPO_TRANSACION.Acreditacion; // ESTA ES LA ACUMULACION POR VENTA 

            filtroTransacciones = "";
            if(!R.isEmpty(filtroFecha)) {
                let fechaInicio = filtroFecha[0].replace('/','-') + ' 00:00:00';
                let fechaFin = filtroFecha[1].replace('/','-') + ' 23:59:59';
                filtroTransacciones += ` AND t.created_at BETWEEN '${fechaInicio}' AND '${fechaFin}'`;
            }
            if(!R.isEmpty(filtroSucursal)) {
                let implodeSucursales = filtroSucursal.join(',');
                filtroTransacciones += ` AND t.id_sucursal IN(${filtroSucursal})`;
            }
            if(!R.isEmpty(filtroProducto)) {
                filtroProducto.forEach( p => {
                      filtroTransacciones += `AND tf.filtro_productos LIKE '%<<--${p}-->>%'`;
                })
            }

            queryAdicional = `INNER JOIN (
            SELECT DISTINCT id_usuario FROM ${tablaTransacciones} AS t 
            LEFT JOIN (
            SELECT f.id, info.filtro_productos
            FROM pl_factura AS f
            INNER JOIN (
            SELECT lf.id_factura, GROUP_CONCAT(CONCAT('<<--', lf.sku_producto, '-->>') SEPARATOR ' ') AS filtro_productos
            FROM pl_linea_factura AS lf GROUP BY lf.id_factura
            ) AS info ON(info.id_factura=f.id) ) AS tf ON(tf.id=t.id_factura)
            WHERE t.tipo IN(${tipoAcumulacion}) ${filtroTransacciones}) AS tmp ON(tmp.id_usuario=u1.id)`;
        }
        if(filtroVentas == 2){
            //cupones
            let filtroCupones = "";
            if(!R.isEmpty(filtroFecha)) {
                let fechaInicio = filtroFecha[0].replace('/','-') + ' 00:00:00';
                let fechaFin = filtroFecha[1].replace('/','-') + ' 23:59:59';
                filtroCupones += ` AND tc.created_at BETWEEN '${fechaInicio}' AND '${fechaFin}'`;
            }
            if(!R.isEmpty(filtroSucursal)) {
                let implodeSucursales = filtroSucursal.join(',');
                filtroCupones += ` AND tc.id_sucursal IN(${filtroSucursal})`;
            }
            if(!R.isEmpty(filtroCupon)) {
                let implodeCupones = filtroCupon.join(',');
                filtroCupones += `AND cc.id_cupon= IN(${implodeCupones})`;
            }

            queryAdicional = `INNER JOIN (
            SELECT DISTINCT tc.id_usuario FROM pl_transaccion_cupon AS tc
            INNER JOIN pl_codigo_cupon AS cc ON(cc.id=tc.id_codigo_cupon)
            WHERE 1=1 ${filtroCupones}) AS tmp ON(tmp.id_usuario=u1.id)`;
        }
        //filtros generales
        //idioma
        if (isValidValue(idioma)) {
            filtro += ` AND u1.id_idioma='${idioma}'`;
        }
        //genero
        if (isValidValue(genero)) {
            let filtroGenero = [];
            let keyND = genero.indexOf('-1'); 
            if (keyND !== -1) {
                genero = R.without(['-1'],genero);
                filtroGenero.push("u1.id_genero IS NULL");
            }
            if (!R.isEmpty(genero)) {
                let implodeGenero =  genero.join(',');
                filtroGenero.push(`u1.id_genero IN('${implodeGenero}')`);
            }
            let implodeFiltroGenero = filtroGenero.join(' OR ');
            filtro += ` AND (${implodeFiltroGenero})`;
        }
        //edad
        if (isValidValue(edad)) {
            let filtroEdad = [];
            if (R.includes('men_18',edad)) {
                filtroEdad.push("u1.edad>0 AND u1.edad<18");
            }
            if (R.includes('18_24',edad)) {
                filtroEdad.push("u1.edad>=18 AND u1.edad<=24");
            }
            if (R.includes('25_35',edad)) {
               filtroEdad.push("u1.edad>=25 AND u1.edad<=35");
            }
            if (R.includes('36_45',edad)) {
               filtroEdad.push("u1.edad>=36 AND u1.edad<=45");
            }
            if (R.includes('46_55',edad)) {
                filtroEdad.push("u1.edad>=46 AND u1.edad<=55");
            }
            if (R.includes('56_65',edad)) {
               filtroEdad.push("u1.edad>=56 AND u1.edad<=65");
            }
            if (R.includes('may_65',edad)) {
                filtroEdad.push("u1.edad>65");
            }

            let implodeFiltroEdad = filtroEdad.join(') OR (');
            filtro += ` AND ((${implodeFiltroEdad})) `;
        }
        //nivel
        if (isValidValue(nivel)) {
            let implodeNivel = nivel.join(',')
            filtro += ` AND u1.id_nivel IN('${implodeNivel}')`;
        }

        const query = `SELECT u1.sub_cognito AS ${SUB_COGNITO} FROM (
        SELECT u.id, u.sub_cognito, u.id_nivel, u.id_idioma, u.id_genero, TIMESTAMPDIFF(YEAR,u.fecha_nacimiento,CURDATE()) AS edad
        FROM pl_usuario AS u WHERE u.estado=1 AND u.sub_cognito IS NOT NULL) AS u1 
        ${queryAdicional} WHERE 1=1 ${filtro}`;

        const result = await sequelizeCliente.query(query, { type: sequelizeCliente.QueryTypes.SELECT});
        return { valido: true, data: result};
};

const getCampaingUsersBySegment = async ({ sequelizeCliente, campaignIdPinpoint }) => {
    console.log(`[getCampaingUsersBySegment]`)
    const query = ` 
        SELECT 
            not_campania.id_campania as id_campania_lealto,  
            sub_cognito as ${SUB_COGNITO}
        FROM 
            not_campania_filtros_subs
        INNER JOIN
            not_campania
        ON
            not_campania.id=not_campania_filtros_subs.id_campania
        WHERE
            not_campania.id_campania = ?
    `
    const result = await sequelizeCliente.query(query, { replacements: [campaignIdPinpoint], type: sequelizeCliente.QueryTypes.SELECT});
    console.log({ getCampaingUsersBySegment: JSON.stringify(result)});
    return { valido: true, data: result};
};

const getCampaingDetails = async ({ sequelizeCliente, configCliente, campaignIdPinpoint }) => {
    if(!isValidValue(sequelizeCliente)){
        return { valido: false, error: 'Invalid sequelizeCliente'};
    }
    const query = ` 
        SELECT 
            not_campania.id_campania as id_campania_lealto, 
            tipo, 
            generos, 
            edades, 
            niveles, 
            rango_fechas, 
            sucursales, 
            ventas, 
            sku_productos, 
            not_campania.id_idioma as idioma, 
            cupones 
        FROM 
            not_campania_filtros 
        INNER JOIN
            not_campania
        ON
            not_campania.id=not_campania_filtros.id_campania
        WHERE
            not_campania.id_campania = ?`
    const result = await sequelizeCliente.query(query, { replacements: [campaignIdPinpoint], type: sequelizeCliente.QueryTypes.SELECT});
    const formatResult = R.head(result);
    console.log({ getCampaingDetails: JSON.stringify(formatResult)});
    return formatResult;
};

const filterEndpoints = (endpoints = {},subsCognito = []) => {
    const getSugCognito = R.pluck(SUB_COGNITO);
    const subs = getSugCognito(subsCognito);
    const finalEndpoints = {}
    for (var key in endpoints) { // esto se asi para que no consuma tanta memoria
        if (endpoints.hasOwnProperty(key)) {
            const endpoint = endpoints[key];
            const subCognito = R.pathOr(null, ['User', 'UserId'], endpoint);
            const available = R.includes(subCognito,subs);
            if(available){
                finalEndpoints[key] = endpoint;  
            }
        }
    }
    return finalEndpoints;
};

module.exports.handler = filterSegmentHandler;

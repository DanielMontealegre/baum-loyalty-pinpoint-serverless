//packages
const response = require("cfn-response");
const Sequelize = require("sequelize");

//Helpers
const ConfigHelper = require("../helpers/Config.helper.js");
const SSMHelper = require("../helpers/Enviroment.helper.js");
//db
const sequelizeToInit = require("../core/Sequelize.js");
//handler

const updateAWSConfigLealto = (event, context) => {
  console.log(
    `=======event updateAWSConfigLealto==> ${JSON.stringify(
      event
    )} ==================>`
  );
  try {
    let responseData = { lealtoConfigUpdated: false };
    let conexionDbMaster;
    const {
      sdkRoleArn,
      cognitoUserPoolId,
      pinpointAppId,
      idClienteLealto,
      lambdaNameFilterCampaign
    } = event.ResourceProperties;

    SSMHelper.getDBConfig()
      .then(resultConfig => {
        console.log("--- CALL 1 ---");
        if (!resultConfig.valid) {
          throw new Error(
            "Couldnt load enviroment variables! Need valid .env for SSM or local"
          );
        }
        return resultConfig.databaseConfig;
      })
      .then(databaseConfig => {
        console.log("--- CALL 2 ---");
        conexionDbMaster = sequelizeToInit.init(databaseConfig);
        return conexionDbMaster.authenticate();
      })
      .then(() => {
        console.log("--- CALL 3 ---");
        if (event.RequestType == "Delete") {
          console.log("--- DELETE CALL ---");
          return updateConfig(
            conexionDbMaster,
            null,
            null,
            null,
            null,
            idClienteLealto
          ).then(() => response.send(event, context, response.SUCCESS));
        }
        console.log("--- CALL 4 ---");
        if (event.RequestType == "Create" || event.RequestType == "Update") {
          console.log(`--- ${event.RequestType} CALL ---`);
          return updateConfig(
            conexionDbMaster,
            sdkRoleArn,
            cognitoUserPoolId,
            pinpointAppId,
            lambdaNameFilterCampaign,
            idClienteLealto
          ).then(result => {
            responseData.lealtoConfigUpdated = result ? true : false;
            response.send(event, context, response.SUCCESS, responseData);
          });
        }
        console.log("--- CALL 5 ---");
        response.send(event, context, response.SUCCESS, responseData);
      })
      .catch(error => {
        console.log(error);
        responseData = { Error: error };
        response.send(event, context, response.SUCCESS, responseData);
      });
  } catch (error) {
    console.log(error);
    responseData = { Error: error };
    response.send(event, context, response.SUCCESS, responseData);
  }
};

//operations on lealto db
const updateConfig = (
  conexionDbMaster,
  sdkRoleArn = null,
  cognitoUserPoolId = null,
  pinpointAppId = null,
  lambdaNameFilterCampaign = null,
  idClienteLealto
) => {
  const aws_analytics =
    sdkRoleArn && cognitoUserPoolId && pinpointAppId ? true : false;
  return conexionDbMaster.query(
    `
                UPDATE config_aws inner join cliente on config_aws.id = cliente.id_config_aws  
                SET 
                config_aws.aws_analytics = ?, 
                config_aws.aws_analytics_arn_role = ?, 
                config_aws.cognito_user_pool_id = ?, 
                config_aws.pinpoint_application_id = ?,
                config_aws.pinpoint_filter_lambda_func = ? 
                where cliente.id = ?; `,
    {
      replacements: [
        aws_analytics,
        sdkRoleArn,
        cognitoUserPoolId,
        pinpointAppId,
        lambdaNameFilterCampaign,
        idClienteLealto
      ],
      type: Sequelize.QueryTypes.UPDATE
    }
  );
};

module.exports.handler = updateAWSConfigLealto;

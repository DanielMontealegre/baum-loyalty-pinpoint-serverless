//Helpers
const ConfigHelper = require("../helpers/Config.helper.js");
const sequelizeInit = require("../core/Sequelize.js");

//Controller
const UsuarioController = require("../controllers/Usuario.controller.js");

const getDataFromEvent = event => {
  const userAttributes = event.request.userAttributes;
  const params = {
    sub: userAttributes["sub"],
    codigo_invitacion: userAttributes["custom:invitationCode"],
    email: userAttributes["email"],
    identificacion: userAttributes["custom:identificacion"],
    codigo_preregistro: userAttributes["custom:preRegisterCode"],
    tipo_identificacion: userAttributes["custom:typeID"],
    nombre: userAttributes["name"],
    imagen_perfil: userAttributes["photo"],
    apellidos: userAttributes["family_name"],
    telefono: userAttributes["phone_number"],
    fecha_nacimiento: userAttributes["birthdate"],
    id_genero: userAttributes["gender"],
    id_idioma: userAttributes["custom:id_idioma"]
      ? userAttributes["custom:id_idioma"]
      : "es"
  };
  return params;
};

const postConfirmationHandler = async (event, dbConfig) => {
  console.log(`=======event==> ${JSON.stringify(event)} ==================>`);
  try {
    const data = getDataFromEvent(event);
    const result = await ConfigHelper.getConfigCliente(event, dbConfig);

    if (!result.valido) {
      throw new Error(JSON.stringify(result));
      return event;
    }
    const sequelizeMasterCon = sequelizeInit.init(dbConfig);
    const { sequelizeCliente, config_cliente } = result;
    const controllerUsuario = new UsuarioController(
      sequelizeCliente,
      sequelizeMasterCon,
      config_cliente,
      data.id_idioma
    );
    const response = await controllerUsuario.AppRegistrarUsuario(data);
    if (!response.valido) {
      throw new Error(JSON.stringify(response));
      return;
    }
    console.log(
      `====Response===event==> ${JSON.stringify(event)} ==================>`
    );
    return event;
  } catch (error) {
    console.log(error);
    throw new Error(
      JSON.stringify({
        valido: false,
        status: 500,
        error: { mensaje: "Error inesperado - 0002" }
      })
    );
    return;
  }
};

const evaluateBooleanString = value => {
  if (value == "1") {
    return true;
  }
  if (value == 1) {
    return true;
  }
  if (value === true) {
    return true;
  } else {
    return false;
  }
};

module.exports.handler = postConfirmationHandler;

/*
//para text local
const eventExample = {
    "version": "1",
    "region": "us-east-1",
    "userPoolId": "us-east-1_tB9jKybPP",
    "userName": "baumteam@mailinator.com",
    "callerContext": {
        "awsSdkVersion": "aws-sdk-unknown-unknown",
        "clientId": "crrq6dsaplhahtea78d6haml6"
    },
    "triggerSource": "PostConfirmation_ConfirmSignUp",
    "request": {
        "userAttributes": {
            "sub": "54d67b92-b0a9-44b5-bc9c-ef0ab322b1ce",
            "cognito:user_status": "CONFIRMED",
            "email_verified": "true",
            "custom:isSocial": "0",
            "name": "harley",
            "family_name": "espinoza",
            "custom:receiveNotifications": "1",
            "email": "baumteam@mailinator.com"
        }
    },
    "response": {}
};

postConfirmationHandler(eventExample).then(result => {
	console.log(JSON.stringify(result));
	return result;
}).catch(err => {
	console(err);
})

*/

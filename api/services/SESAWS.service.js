//Variables
const Config = require('../../config.js'),
    ConfigAWSSDK = Config.SDKs.sdk_aws_config,
    SESAWSConfig = Config.Email.ses_aws_config,
    utf8 = require('utf8'),
    AWS = require('aws-sdk');

//Configuracion
AWS.config.update(ConfigAWSSDK); 
let SES = new AWS.SES({
    apiVersion: SESAWSConfig.version
});

/*
 * Función para realizar envíos por correo
 * parametros:
 *  to-> Array of objects: [{email: ''{, name: ''}}, {email: ''{, name: ''}}, ...]
 *  subject-> Text
 *  body-> HTML Text
 *  from-> Object: {email: ''{, name: ''}}
 */
module.exports.sendMail = (config_cliente,to, subject, body, from, callBack) => {
    if (!from) {
        from = (config_cliente.AWSConfig) ? {
                email: config_cliente.AWSConfig.ses_default_from_email,
                name: utf8.encode( ( typeof config_cliente.AWSConfig.ses_default_from_name != 'undefined')?  config_cliente.AWSConfig.ses_default_from_name  : "" )
            }

            :
            SESAWSConfig.default_from;
    }
    let message_params = {
        Destination: { ToAddresses: getEmailsString(to) },
        Message: {
            Body: { Html: { Data: body } },
            Subject: { Data: subject }
        },
        Source: emailObjectToString(from)
    };
    return SES.sendEmail(message_params).promise().catch(err => {
        console.log(err);
        return null;
    });

};

function getEmailsString(objects) {
    let data = [];
    objects.forEach( (object) =>  {
        data.push(emailObjectToString(object));
    });
    return data;
}

/* Formato Nombre <Email> */
function emailObjectToString(object) {
    if (object.name && object.name !== '') {
        return '"' + object.name + '" <' + object.email + '>';
    } else {
        return object.email;
    }
}
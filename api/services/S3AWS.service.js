//Variables
const Config = require('../../config.js'),
    ConfigAWSSDK = Config.SDKs.sdk_aws_config,
    S3AWSConfig = Config.Storage.s3_aws_config,
    AWS = require('aws-sdk');

AWS.config.update(ConfigAWSSDK); 




const putObject = async(bucketDir,Key, Body,ACL = 'public-read', ContentType = 'image/gif',ContentEncoding = 'base64') => {
    try {
        const data = {
            Key,
            Body,
            ContentType,
            ContentEncoding,
        };

        const s3Bucket = new AWS.S3({
            params: {
                Bucket: bucketDir,
                ACL: ACL
            }
        });


        return s3Bucket.putObject(data)
            .promise()
            .then(result => true)
            .catch(error => {
                console.log(`Error putObject ${error}`);
                return false;
            })
    } catch (error) {
        console.log(`Error catch putObject ${error}`);
        return false;
    }

};

const getObject = async(Key, Bucket,bucketDir) => {
    try {
        const params = {
            Bucket,
            Key
        };

        const s3Bucket = new AWS.S3({
            params: {
                Bucket: bucketDir,
                ACL: 'authenticated-read'
            }
        });

        return s3Bucket.getObject(params)
            .promise()
            .then(data => {
                return JSON.parse(data.Body);
            })
            .catch(error => {
                console.log(`Error getObject ${error}`);
                return null;
            })
    } catch (error) {
        console.log(`Error catch getObject ${error}`);
        return false;
    }

}

const deleteObject = async(Key, Bucket,bucketDir) => {
    try {
        const params = {
            Bucket,
            Key
        };
        const s3Bucket = new AWS.S3({
            params: {
                Bucket: bucketDir,
                ACL: 'authenticated-read'
            }
        });

        return s3Bucket.deleteObject(params)
            .promise()
            .then(result => true)
            .catch(error => {
                console.log(`Error deleteObject ${error}`);
                return false;
            })
    } catch (error) {
        console.log(`Error catch deleteObject ${error}`);
        return false;
    }

}

module.exports.putObject = putObject;
module.exports.getObject = getObject;
module.exports.deleteObject = deleteObject;

/**
 * [saveImage description]
 * @param  {[type]} img         [description]
 * @param  {[type]} name        [description]
 * @param  {[type]} contentType [description]
 * @return {[type]}             [description]
 */
module.exports.saveImage = (config_cliente,img, name, contentType, ident_usuario, ident_cliente, callBack) => {
    let buf = new Buffer(img, 'base64');
    let data = {
        Key: name,
        Body: buf,
        ContentEncoding: 'base64',
        ContentType: contentType
    };

    let nombre_empresa = config_cliente.nombre_empresa; 
    let direccion = config_cliente.AWSConfig.s3_bucket + "/usuarios/" + "usuario" + "-" + ident_usuario + "/imagenes-perfil";
    
    let direccion_pruebas = "bucket-baum-loyalty/pruebas_qr_codigo_alfa"

    let s3Bucket = new AWS.S3({
        params: {
            Bucket: direccion ,
            ACL: 'public-read'
        }
    });


    s3Bucket.putObject(data, (err, data) =>  {
        if (err) {
            console.log(err);
            console.log('Error uploading data: ', data);
            callBack(err);
            return;
        } else {
            console.log('succesfully uploaded the image!');
            callBack();
            return;
        }
    });
};

/**
 * [deleteImage description]
 * @param  {[type]} file_dir [description]
 * @return {[type]}          [description]
 */
module.exports.deleteFile = (config_cliente,file) =>  {
    let direccion = config_cliente.AWSConfig.s3_bucket + "/usuarios/" + "usuario" + "-" + ident_usuario + "/imagenes-perfil";
    let params = {
        Key: file
    };
    let s3Bucket = new AWS.S3({
        params: {
            Bucket: direccion ,
            ACL: 'public-read'
        }
    });
    s3Bucket.deleteObject(params, (err, data) =>  {
        if (data) {
            console.log("File deleted successfully");
        } else {
            console.log("Check if you have sufficient permissions : " + err);
        }
    });
};
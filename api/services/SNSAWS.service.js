//Variables
const Config = require('../../config.js'),
    ConfigAWSSDK = Config.SDKs.sdk_aws_config,
    ConfigAWSSNS = Config.Notification.sns_aws_config,
    AWS = require('aws-sdk');

//Configuracion
AWS.config.update(ConfigAWSSDK); 
let SNS = new AWS.SNS({
    apiVersion: ConfigAWSSNS.version
});

/*
 * Función para crear un endpoint en SNS AWS
 * parametros:
 *  deviceId-> STRING
 *  platform-> STRING
 *  lang-> STRING
 *  callBack-> FUNCTION
 */
module.exports.createEndpoint = (config_cliente,deviceId, platform, lang, callBack) => {
    let params = {
        Token: deviceId
    };
    let ARNConfig = config_cliente.AWSSNSConfig .arns[lang];
    if (platform === 'iOS') {
        params.PlatformApplicationArn = ARNConfig.ios_arn;
    } else if (platform === 'Android') {
        params.PlatformApplicationArn = ARNConfig.android_arn;
    }
    SNS.createPlatformEndpoint(params, (error, data) =>  {
        if (error) {
            callBack(null, error);
        } else {
            callBack(data);
        }
    });
};

/*
 * Función para suscribir un endpoint al topic en SNS AWS
 * parametros:
 *  deviceId-> STRING
 *  platform-> STRING
 *  lang-> STRING
 *  callBack-> FUNCTION
 */
module.exports.subscribeEndpoint = (config_cliente,endpoint, lang, callBack) =>  {
    let ARNConfig = config_cliente.AWSSNSConfig.arns[lang];

    let params = {
        Protocol: 'application',
        TopicArn: ARNConfig.topic_arn,
        Endpoint: endpoint
    };
    SNS.subscribe(params, (error, data) => {
        if (error) {
            callBack(null, error);
        } else {
            callBack(data);
        }
    });
};

/*
 * Función para suscribir un endpoint al topic de invitados en SNS AWS
 * parametros:
 *  deviceId-> STRING
 *  platform-> STRING
 *  lang-> STRING
 *  callBack-> FUNCTION
 */
module.exports.subscribeEndpointGuest = (config_cliente,endpoint, lang, callBack) => {
    let ARNConfig = config_cliente.AWSSNSConfig .arns[lang];
    let params = {
        Protocol: 'application',
        TopicArn: ARNConfig.guest_topic_arn,
        Endpoint: endpoint
    };
    SNS.subscribe(params, (error, data) => {
        if (error) {
            callBack(null, error);
        } else {
            callBack(data);
        }
    });
};

/*
 * Función para desvincularse de un topic en SNS AWS
 * parametros:
 *  deviceId-> STRING
 *  platform-> STRING
 *  lang-> STRING
 *  callBack-> FUNCTION
 */
module.exports.unsubscribeEndpoint = (config_cliente,subscriptionArn, callBack) => {
    let ARNConfig = config_cliente.AWSSNSConfig.arns[lang];
    let params = {
        SubscriptionArn: subscriptionArn
    };
    SNS.unsubscribe(params, (error, data) =>  {
        if (error) {
            callBack(null, error);
        } else {
            callBack(data);
        }
    });
};

/*
 * Función para realizar un push en SNS AWS
 * parametros:
 *  deviceId-> STRING
 *  platform-> STRING
 *  lang-> STRING
 *  callBack-> FUNCTION
 */
module.exports.publish = (params, callBack) => {
    SNS.publish(params, (error, data) => {
        if (error) {
            callBack(null, error);
        } else {
            callBack(data);
        }
    });
};

